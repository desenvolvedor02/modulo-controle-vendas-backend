package br.com.telugo.controlevendas.repositories;

import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.telugo.controlevendas.enums.FormaPagamento;
import br.com.telugo.controlevendas.enums.TipoUsuario;
import br.com.telugo.controlevendas.models.Login;
import br.com.telugo.controlevendas.models.Produto;
import br.com.telugo.controlevendas.models.Usuario;
import br.com.telugo.controlevendas.models.ValidacaoVenda;
import br.com.telugo.controlevendas.models.Venda;

public interface ValidacaoVendaRepositoryCustom {
	Page<ValidacaoVenda> buscarValidacaoVendasPorFiltros(Pageable pageable, Long id, String nomeBanco,
			String numeroDocumento, String nomePagador,
			Float valorPagamento,  Date dataPagamentoInicio, Date dataPagamentoFim);
}
