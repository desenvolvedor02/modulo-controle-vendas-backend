package br.com.telugo.controlevendas.repositories;

import java.util.Date;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.telugo.controlevendas.enums.StatusLogin;
import br.com.telugo.controlevendas.enums.TipoLogin;
import br.com.telugo.controlevendas.models.Login;
import br.com.telugo.controlevendas.models.Produto;
import br.com.telugo.controlevendas.models.Usuario;



public interface LoginRepositoryCustom {
	Page<Login> buscarLoginsPorFiltros(Pageable pageable, Long id, Usuario usuario,
			Produto produto, String nomeUsuario,TipoLogin tipo,
			Date dataCriacaoInicio, Date dataCriacaoFim, 
			Date dataValidadeInicio, Date dataValidadeFim,
			StatusLogin status);
	
}
