package br.com.telugo.controlevendas.repositories;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.query.QueryUtils;

import br.com.telugo.controlevendas.enums.StatusLogin;
import br.com.telugo.controlevendas.enums.TipoLogin;
import br.com.telugo.controlevendas.enums.TipoUsuario;
import br.com.telugo.controlevendas.models.Login;
import br.com.telugo.controlevendas.models.Produto;
import br.com.telugo.controlevendas.models.Usuario;

public class LoginRepositoryCustomImpl implements LoginRepositoryCustom {
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public Page<Login> buscarLoginsPorFiltros(Pageable pageable, Long id, Usuario usuario,
			Produto produto, String nomeUsuario,TipoLogin tipo,  Date dataCriacaoInicio, Date dataCriacaoFim,
			Date dataValidadeInicio, Date dataValidadeFim,
			StatusLogin status)  {
		//TODO
		CriteriaBuilder criteriaBuilder  = entityManager.getCriteriaBuilder();
		CriteriaQuery<Login> criteriaQuery   = criteriaBuilder.createQuery(Login.class);
		Root login = criteriaQuery.from(Login.class);
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if(id!=null) {
			predicates.add(criteriaBuilder.equal(login.get("id"), id));
		}
		
		//TODO 
		if(usuario!=null) {
			predicates.add(criteriaBuilder.equal(login.get("usuario"), usuario));
		}
		//TODO 
		if(produto!=null) {
			predicates.add(criteriaBuilder.equal(login.get("produto"), produto));
		}
		
		
		if(nomeUsuario!=null && nomeUsuario!="" && nomeUsuario!=" ") {
			predicates.add(criteriaBuilder.like(login.get("nomeUsuario"), "%"+nomeUsuario+"%"));
		}
		
		if(tipo!=null) {
			predicates.add(criteriaBuilder.equal(login.get("tipo"), tipo));
		}
		//TODO refinar essa busca por data
		if(dataCriacaoInicio!=null && dataCriacaoFim!=null) {
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(login.get("dataCriacao"), dataCriacaoInicio));
			predicates.add(criteriaBuilder.lessThanOrEqualTo(login.get("dataCriacao"), dataCriacaoFim));
		}
		if(dataValidadeInicio!=null && dataValidadeFim!=null ) {
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(login.get("dataValidade"), dataValidadeInicio));
			predicates.add(criteriaBuilder.lessThanOrEqualTo(login.get("dataValidade"), dataValidadeFim));
		}
		
		if(status!=null) {
			predicates.add(criteriaBuilder.equal(login.get("status"), status));
		}	
		
		
		
		criteriaQuery.where(predicates.toArray(new Predicate[0]));
		criteriaQuery.orderBy(QueryUtils.toOrders(pageable.getSort(), login, criteriaBuilder));
		
		TypedQuery query = entityManager.createQuery(criteriaQuery);

		int totalRows = query.getResultList().size();

		  query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
		  query.setMaxResults(pageable.getPageSize());
	
		  Page result = new PageImpl(query.getResultList(), pageable, totalRows);
		
		return result;
	}



}
