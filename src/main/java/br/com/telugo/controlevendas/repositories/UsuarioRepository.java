package br.com.telugo.controlevendas.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.telugo.controlevendas.models.Produto;
import br.com.telugo.controlevendas.models.Usuario;




@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>, UsuarioRepositoryCustom{
	Optional<Usuario> findById(long id);
	
	Optional<Usuario> findByNome(String nome);
}
