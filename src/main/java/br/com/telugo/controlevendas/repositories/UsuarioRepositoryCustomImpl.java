package br.com.telugo.controlevendas.repositories;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.query.QueryUtils;

import br.com.telugo.controlevendas.enums.TipoUsuario;
import br.com.telugo.controlevendas.models.Produto;
import br.com.telugo.controlevendas.models.Usuario;

public class UsuarioRepositoryCustomImpl implements UsuarioRepositoryCustom {
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public Page<Usuario> buscarUsuariosPorFiltros(Pageable pageable, Long id, String nome, String cpf, String email,
			String telefone, TipoUsuario tipo, Boolean seParceiroF5, String codigoConsigna, String filial,
			String cidade, String estado)  {
		//TODO
		CriteriaBuilder criteriaBuilder  = entityManager.getCriteriaBuilder();
		CriteriaQuery<Usuario> criteriaQuery   = criteriaBuilder.createQuery(Usuario.class);
		Root usuario = criteriaQuery.from(Usuario.class);
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if(id!=null) {
			predicates.add(criteriaBuilder.equal(usuario.get("id"), id));
		}
		
		if(nome!=null && nome!="" && nome!=" ") {
			predicates.add(criteriaBuilder.like(usuario.get("nome"), "%"+nome+"%"));
		}
		
		if(cpf!=null && cpf!="" && cpf!=" ") {
			predicates.add(criteriaBuilder.like(usuario.get("cpf"), "%"+cpf+"%"));
		}
		
		if(email!=null && email!="" && email!=" ") {
			predicates.add(criteriaBuilder.like(usuario.get("email"), "%"+email+"%"));
		}
		
		if(telefone!=null && telefone!="" && telefone!=" ") {
			predicates.add(criteriaBuilder.like(usuario.get("telefone"), "%"+telefone+"%"));
		}
		if(tipo!=null) {
			predicates.add(criteriaBuilder.equal(usuario.get("tipo"), tipo));
		}
		
		if(seParceiroF5!=null) {
			predicates.add(criteriaBuilder.equal(usuario.get("seParceiroF5"), seParceiroF5));
		}
		
		if(codigoConsigna !=null) {
			predicates.add(criteriaBuilder.equal(usuario.get("codigoConsigna"), codigoConsigna));
		}
		
		if(filial!=null && filial!="" && filial!=" ") {
			predicates.add(criteriaBuilder.like(usuario.get("filial"), "%"+filial+"%"));
		}
		
		if(cidade!=null && cidade!="" && cidade!=" ") {
			predicates.add(criteriaBuilder.like(usuario.get("cidade"), "%"+cidade+"%"));
		}
		
		if(estado!=null && estado!="" && estado!=" ") {
			predicates.add(criteriaBuilder.like(usuario.get("estado"), "%"+estado+"%"));
		}
		
		
		
		criteriaQuery.where(predicates.toArray(new Predicate[0]));
		criteriaQuery.orderBy(QueryUtils.toOrders(pageable.getSort(), usuario, criteriaBuilder));
		
		TypedQuery query = entityManager.createQuery(criteriaQuery);

		int totalRows = query.getResultList().size();

		  query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
		  query.setMaxResults(pageable.getPageSize());
	
		  Page result = new PageImpl(query.getResultList(), pageable, totalRows);
		
		return result;
	}



}
