package br.com.telugo.controlevendas.repositories;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.query.QueryUtils;

import br.com.telugo.controlevendas.enums.FormaPagamento;
import br.com.telugo.controlevendas.models.Login;
import br.com.telugo.controlevendas.models.ValidacaoVenda;
import br.com.telugo.controlevendas.models.Venda;

public class ValidacaoVendaRepositoryCustomImpl implements ValidacaoVendaRepositoryCustom {
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public Page<ValidacaoVenda> buscarValidacaoVendasPorFiltros(Pageable pageable, Long id, String nomeBanco,
			String numeroDocumento, String nomePagador,
			Float valorPagamento,  Date dataPagamentoInicio, Date dataPagamentoFim){
		
		CriteriaBuilder criteriaBuilder  = entityManager.getCriteriaBuilder();
		CriteriaQuery<Venda> criteriaQuery   = criteriaBuilder.createQuery(Venda.class);
		Root venda = criteriaQuery.from(Venda.class);
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if(id!=null) {
			predicates.add(criteriaBuilder.equal(venda.get("id"), id));
		}
			
		if(nomeBanco!=null && nomeBanco!="" && nomeBanco!=" ") {
			predicates.add(criteriaBuilder.like(venda.get("nomeBanco"), "%"+nomeBanco+"%"));
		}	
		if(numeroDocumento!=null && numeroDocumento!="" && numeroDocumento!=" ") {
			predicates.add(criteriaBuilder.like(venda.get("numeroDocumento"), "%"+numeroDocumento+"%"));
		}
		
		if(nomePagador!=null && nomePagador!="" && nomePagador!=" ") {
			predicates.add(criteriaBuilder.like(venda.get("nomePagador"), "%"+nomePagador+"%"));
		}	
		if(valorPagamento!=null) {
			predicates.add(criteriaBuilder.equal(venda.get("valorPagamento"), valorPagamento));
		}	
	
		
		if(dataPagamentoInicio!=null && dataPagamentoFim!=null) {			
			Calendar c = Calendar.getInstance();
			 c.setTime(dataPagamentoInicio);
			 System.out.println(dataPagamentoInicio);
			 System.out.println("Dia do Mês: "+c.get(Calendar.DAY_OF_MONTH));
			 System.out.println(" Mês: "+c.get(Calendar.MONTH));
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(venda.get("dataPagamento"), dataPagamentoInicio));
			predicates.add(criteriaBuilder.lessThanOrEqualTo(venda.get("dataPagamento"), dataPagamentoFim));
		}			
		
		criteriaQuery.where(predicates.toArray(new Predicate[0]));
		criteriaQuery.orderBy(QueryUtils.toOrders(pageable.getSort(), venda, criteriaBuilder));
		
		TypedQuery query = entityManager.createQuery(criteriaQuery);

		int totalRows = query.getResultList().size();

		  query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
		  query.setMaxResults(pageable.getPageSize());
	
		  Page result = new PageImpl(query.getResultList(), pageable, totalRows);
		
		return result;
	}
	
	

}
