package br.com.telugo.controlevendas.repositories;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.query.QueryUtils;

import br.com.telugo.controlevendas.models.Produto;

public class ProdutoRepositoryCustomImpl implements ProdutoRepositoryCustom {
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public Page<Produto> buscarProdutosPorFiltros(Pageable pageable, Long id, String nome, Float valorCompra,
			Float valorVenda, Integer duracaoEmDias, String fornecedor, Float margemDeGanho) {
		
		CriteriaBuilder criteriaBuilder  = entityManager.getCriteriaBuilder();
		CriteriaQuery<Produto> criteriaQuery   = criteriaBuilder.createQuery(Produto.class);
		Root produto = criteriaQuery.from(Produto.class);
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if(id!=null) {
			predicates.add(criteriaBuilder.equal(produto.get("id"), id));
		}
		
		if(nome!=null && nome!="" && nome!=" ") {
			predicates.add(criteriaBuilder.like(produto.get("nome"), "%"+nome+"%"));
		}
		if(valorCompra!=null) {
			predicates.add(criteriaBuilder.equal(produto.get("valorCompra"), valorCompra));
		}
		if(valorVenda!=null) {
			predicates.add(criteriaBuilder.equal(produto.get("valorVenda"), valorVenda));
		}
		if(duracaoEmDias!=null) {
			predicates.add(criteriaBuilder.equal(produto.get("duracaoEmDias"), duracaoEmDias));
		}
		if(fornecedor!=null && fornecedor!="" && fornecedor!=" ") {
			predicates.add(criteriaBuilder.like(produto.get("fornecedor"), "%"+fornecedor+"%"));
		}
		if(margemDeGanho!=null) {
			predicates.add(criteriaBuilder.equal(produto.get("margemDeGanho"), margemDeGanho));
		}
		
		criteriaQuery.where(predicates.toArray(new Predicate[0]));
		criteriaQuery.orderBy(QueryUtils.toOrders(pageable.getSort(), produto, criteriaBuilder));
		
		TypedQuery query = entityManager.createQuery(criteriaQuery);

		int totalRows = query.getResultList().size();

		  query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
		  query.setMaxResults(pageable.getPageSize());
	
		  Page result = new PageImpl(query.getResultList(), pageable, totalRows);
		
		return result;
	}

}
