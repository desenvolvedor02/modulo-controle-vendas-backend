package br.com.telugo.controlevendas.repositories;

import java.util.Date;
import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.telugo.controlevendas.enums.FormaPagamento;
import br.com.telugo.controlevendas.enums.TipoUsuario;
import br.com.telugo.controlevendas.models.Login;
import br.com.telugo.controlevendas.models.Produto;
import br.com.telugo.controlevendas.models.Usuario;
import br.com.telugo.controlevendas.models.Venda;

public interface VendaRepositoryCustom {
	Page<Venda> buscarVendasPorFiltros(Pageable pageable, Long id, Long idParceiro,
			String favorecido, FormaPagamento formaPagamento,
			Float valor,  String conta, String agencia,
			String operacao, List<Login> logins, String observacao, String numeroDocumento
			,Date dataPagamentoInicio, Date dataPagamentoFim);
	
	List<Venda> buscarVendasPorRangeDatas(Date dataPagamentoInicio, Date dataPagamentoFim);
	
}
