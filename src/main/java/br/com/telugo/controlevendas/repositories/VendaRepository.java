package br.com.telugo.controlevendas.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import br.com.telugo.controlevendas.models.Venda;




@Repository
public interface VendaRepository extends JpaRepository<Venda, Long>, VendaRepositoryCustom{
	Optional<Venda> findById(long id);
}
