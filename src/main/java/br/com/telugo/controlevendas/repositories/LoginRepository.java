package br.com.telugo.controlevendas.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.telugo.controlevendas.models.Login;
import br.com.telugo.controlevendas.models.Produto;




@Repository
public interface LoginRepository extends JpaRepository<Login, Long>, LoginRepositoryCustom{
	Optional<Login> findById(long id);
}
