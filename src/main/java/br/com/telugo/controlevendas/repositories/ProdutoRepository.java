package br.com.telugo.controlevendas.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import br.com.telugo.controlevendas.models.Produto;




@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Long> , ProdutoRepositoryCustom{
	Optional<Produto> findById(long id);
	
	Optional<Produto> findByNome(String nome);
}
