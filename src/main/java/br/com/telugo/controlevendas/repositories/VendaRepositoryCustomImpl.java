package br.com.telugo.controlevendas.repositories;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.query.QueryUtils;

import br.com.telugo.controlevendas.enums.FormaPagamento;
import br.com.telugo.controlevendas.models.Login;
import br.com.telugo.controlevendas.models.Venda;

public class VendaRepositoryCustomImpl implements VendaRepositoryCustom {
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public Page<Venda> buscarVendasPorFiltros(Pageable pageable, Long id,
			Long idParceiro,
			String favorecido, FormaPagamento formaPagamento,
			Float valor,  String conta, String agencia,
			String operacao, List<Login> logins, String observacao, String numeroDocumento
			,Date dataPagamentoInicio, Date dataPagamentoFim){
		
		CriteriaBuilder criteriaBuilder  = entityManager.getCriteriaBuilder();
		CriteriaQuery<Venda> criteriaQuery   = criteriaBuilder.createQuery(Venda.class);
		Root venda = criteriaQuery.from(Venda.class);
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if(id!=null) {
			predicates.add(criteriaBuilder.equal(venda.get("id"), id));
		}
		if(idParceiro!=null) {
			predicates.add(criteriaBuilder.equal(venda.get("idParceiro"), idParceiro));
		}		
		if(favorecido!=null && favorecido!="" && favorecido!=" ") {
			predicates.add(criteriaBuilder.like(venda.get("favorecido"), "%"+favorecido+"%"));
		}		
		if(formaPagamento!=null) {
			predicates.add(criteriaBuilder.equal(venda.get("formaPagamento"), formaPagamento));
		}
		if(valor!=null) {
			predicates.add(criteriaBuilder.equal(venda.get("valor"), valor));
		}
		if(conta!=null && conta!="" && conta!=" ") {
			predicates.add(criteriaBuilder.like(venda.get("conta"), "%"+conta+"%"));
		}	
		if(agencia!=null && agencia!="" && agencia!=" ") {
			predicates.add(criteriaBuilder.like(venda.get("agencia"), "%"+agencia+"%"));
		}
		if(operacao!=null && operacao!="" && operacao!=" ") {
			predicates.add(criteriaBuilder.like(venda.get("operacao"), "%"+operacao+"%"));
		}
		//TODO logins
		if(observacao!=null && observacao!="" && observacao!=" ") {
			predicates.add(criteriaBuilder.like(venda.get("observacao"), "%"+observacao+"%"));
		}
		if(numeroDocumento!=null && numeroDocumento!="" && numeroDocumento!=" ") {
			predicates.add(criteriaBuilder.like(venda.get("favorecido"), "%"+favorecido+"%"));
		}	
		
		if(dataPagamentoInicio!=null && dataPagamentoFim!=null) {			
			Calendar c = Calendar.getInstance();
			 c.setTime(dataPagamentoInicio);
			 System.out.println(dataPagamentoInicio);
			 System.out.println("Dia do Mês: "+c.get(Calendar.DAY_OF_MONTH));
			 System.out.println(" Mês: "+c.get(Calendar.MONTH));
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(venda.get("dataPagamento"), dataPagamentoInicio));
			predicates.add(criteriaBuilder.lessThanOrEqualTo(venda.get("dataPagamento"), dataPagamentoFim));
		}			
		
		criteriaQuery.where(predicates.toArray(new Predicate[0]));
		criteriaQuery.orderBy(QueryUtils.toOrders(pageable.getSort(), venda, criteriaBuilder));
		
		TypedQuery query = entityManager.createQuery(criteriaQuery);

		int totalRows = query.getResultList().size();

		  query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
		  query.setMaxResults(pageable.getPageSize());
	
		  Page result = new PageImpl(query.getResultList(), pageable, totalRows);
		
		return result;
	}
	
	@Override
	public List<Venda> buscarVendasPorRangeDatas(Date dataPagamentoInicio, Date dataPagamentoFim){
		
		CriteriaBuilder criteriaBuilder  = entityManager.getCriteriaBuilder();
		CriteriaQuery<Venda> criteriaQuery   = criteriaBuilder.createQuery(Venda.class);
		Root venda = criteriaQuery.from(Venda.class);
		List<Predicate> predicates = new ArrayList<Predicate>();
		
	
		if(dataPagamentoInicio!=null && dataPagamentoFim!=null) {
			
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(venda.get("dataPagamento"), dataPagamentoInicio));
			predicates.add(criteriaBuilder.lessThanOrEqualTo(venda.get("dataPagamento"), dataPagamentoFim));
		}			
		
		criteriaQuery.where(predicates.toArray(new Predicate[0]));
	
		TypedQuery query = entityManager.createQuery(criteriaQuery);

		return query.getResultList();
	}

	

}
