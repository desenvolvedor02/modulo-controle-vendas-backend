package br.com.telugo.controlevendas.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.telugo.controlevendas.models.Produto;

public interface ProdutoRepositoryCustom {
	Page<Produto> buscarProdutosPorFiltros(Pageable pageable, Long id, String nome,
			Float valorCompra, Float valorVenda,Integer duracaoEmDias,  String fornecedor, Float margemDeGanho);
}
