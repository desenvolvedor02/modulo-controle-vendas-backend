package br.com.telugo.controlevendas.repositories;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.telugo.controlevendas.enums.TipoUsuario;
import br.com.telugo.controlevendas.models.Produto;
import br.com.telugo.controlevendas.models.Usuario;

public interface UsuarioRepositoryCustom {
	Page<Usuario> buscarUsuariosPorFiltros(Pageable pageable, Long id, String nome,
			String cpf, String email,String telefone,  TipoUsuario tipo, Boolean seParceiroF5,
			String codigoConsigna, String filial,String cidade,  String estado);
}
