package br.com.telugo.controlevendas.mappers;

import java.util.List;

import org.mapstruct.Mapper;

import br.com.telugo.controlevendas.dtos.UsuarioDTO;
import br.com.telugo.controlevendas.models.Usuario;



@Mapper(componentModel="spring")
public interface UsuarioMapper {
	UsuarioDTO toDto(Usuario usuario);
	Usuario toEntity(UsuarioDTO usuarioDto);
	
	List<UsuarioDTO> toDto(List<Usuario> usuarioList);
	List<Usuario> toEntity(List<UsuarioDTO> usuarioDtoList);
}
