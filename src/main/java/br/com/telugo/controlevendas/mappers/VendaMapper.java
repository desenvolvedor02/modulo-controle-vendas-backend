package br.com.telugo.controlevendas.mappers;

import java.util.List;

import org.mapstruct.Mapper;

import br.com.telugo.controlevendas.dtos.VendaDTO;
import br.com.telugo.controlevendas.models.Venda;



@Mapper(componentModel="spring")
public interface VendaMapper {
	VendaDTO toDto(Venda venda);
	Venda toEntity(VendaDTO vendaDto);
	
	List<VendaDTO> toDto(List<Venda> vendaList);
	List<Venda> toEntity(List<VendaDTO> vendaDtoList);
}
