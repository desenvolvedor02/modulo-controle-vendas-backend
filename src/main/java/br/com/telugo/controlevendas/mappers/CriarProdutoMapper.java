package br.com.telugo.controlevendas.mappers;

import java.util.List;

import org.mapstruct.Mapper;

import br.com.telugo.controlevendas.dtos.CriarProdutoDTO;
import br.com.telugo.controlevendas.models.Produto;



@Mapper(componentModel="spring")
public interface CriarProdutoMapper {
	CriarProdutoDTO toDto(Produto produto);
	Produto toEntity(CriarProdutoDTO produtoDto);
	
	List<CriarProdutoDTO> toDto(List<Produto> produtoList);
	List<Produto> toEntity(List<CriarProdutoDTO> produtoDtoList);
}
