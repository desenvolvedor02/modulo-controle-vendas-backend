package br.com.telugo.controlevendas.mappers;

import java.util.List;

import org.mapstruct.Mapper;

import br.com.telugo.controlevendas.dtos.ProdutoDTO;
import br.com.telugo.controlevendas.models.Produto;



@Mapper(componentModel="spring")
public interface ProdutoMapper {
	ProdutoDTO toDto(Produto produto);
	Produto toEntity(ProdutoDTO produtoDto);
	
	List<ProdutoDTO> toDto(List<Produto> produtoList);
	List<Produto> toEntity(List<ProdutoDTO> produtoDtoList);
}
