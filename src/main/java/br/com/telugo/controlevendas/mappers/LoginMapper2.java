package br.com.telugo.controlevendas.mappers;

import java.util.List;

import org.mapstruct.Mapper;

import br.com.telugo.controlevendas.dtos.LoginDTO;
import br.com.telugo.controlevendas.models.Login;



@Mapper(componentModel="spring")
public interface LoginMapper2 {
	LoginDTO toDto(Login login);
	Login toEntity(LoginDTO loginDto);
	
	List<LoginDTO> toDto(List<Login> loginList);
	List<Login> toEntity(List<LoginDTO> loginDtoList);
}
