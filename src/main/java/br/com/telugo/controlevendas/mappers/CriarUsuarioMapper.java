package br.com.telugo.controlevendas.mappers;

import java.util.List;

import org.mapstruct.Mapper;

import br.com.telugo.controlevendas.dtos.CriarUsuarioDTO;
import br.com.telugo.controlevendas.models.Usuario;



@Mapper(componentModel="spring")
public interface CriarUsuarioMapper {
	CriarUsuarioDTO toDto(Usuario usuario);
	Usuario toEntity(CriarUsuarioDTO usuarioDto);
	
	List<CriarUsuarioDTO> toDto(List<Usuario> usuarioList);
	List<Usuario> toEntity(List<CriarUsuarioDTO> usuarioDtoList);
}
