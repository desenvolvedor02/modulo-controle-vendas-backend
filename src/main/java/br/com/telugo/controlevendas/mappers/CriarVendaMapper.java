package br.com.telugo.controlevendas.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import br.com.telugo.controlevendas.dtos.CriarUsuarioDTO;
import br.com.telugo.controlevendas.dtos.CriarVendaDTO;
import br.com.telugo.controlevendas.models.Venda;



@Mapper(componentModel="spring")
public interface CriarVendaMapper {
	CriarVendaDTO toDto(Venda venda);
	
	//@Mapping(target = "renovacaoLoginsId", ignore = true)
	@Mapping(target = "logins", ignore = true)
	Venda toEntity(CriarVendaDTO vendaDto);
	
	List<CriarVendaDTO> toDto(List<Venda> vendaList);
	List<Venda> toEntity(List<CriarVendaDTO> vendaDtoList);
}
