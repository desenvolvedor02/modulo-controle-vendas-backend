package br.com.telugo.controlevendas.dtos;

import javax.persistence.Basic;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.telugo.controlevendas.enums.StatusLogin;
import br.com.telugo.controlevendas.enums.TipoLogin;
import br.com.telugo.controlevendas.enums.TipoUsuario;
import br.com.telugo.controlevendas.models.Produto;
import br.com.telugo.controlevendas.models.Usuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LoginDTO {
	private long id;		
	private Usuario usuario;
	private Produto produto;
	private String nomeUsuario;
	private TipoLogin tipo;
	private java.util.Date dataCriacao;
	private java.util.Date dataValidade;
	private StatusLogin status;
}
