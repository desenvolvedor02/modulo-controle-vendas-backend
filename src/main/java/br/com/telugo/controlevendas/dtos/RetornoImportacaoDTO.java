package br.com.telugo.controlevendas.dtos;


import java.util.List;

import br.com.telugo.controlevendas.models.ValidacaoVenda;
import br.com.telugo.controlevendas.models.Venda;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RetornoImportacaoDTO {
	List<ValidacaoVendaERRODTO> validacaoVendaErroDTOList;
	List<ValidacaoVenda> validacaoVendaList;
	List<Venda> vendasModificadasList;
}
