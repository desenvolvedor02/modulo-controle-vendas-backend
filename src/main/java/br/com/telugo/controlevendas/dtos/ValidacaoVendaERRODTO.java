package br.com.telugo.controlevendas.dtos;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ValidacaoVendaERRODTO {
	private Integer numeroLinha;	
	private String nomeBanco;
	private String numeroDocumento;
	private String valorPagamento;
	private String nomePagador;
	private String dataPagamento;
}
