package br.com.telugo.controlevendas.dtos;

import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import br.com.telugo.controlevendas.enums.FormaPagamento;
import br.com.telugo.controlevendas.models.Login;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CriarVendaDTO {
	private long idParceiro;
	private String favorecido;
	private FormaPagamento formaPagamento;
	private float valor;
	private String conta;
	private String agencia;
	private String operacao;
	private List<Long> renovacaoLoginsId;
	private List<CriarLoginDTO> novosLogins;
	private String observacao;
	private String comprovantePagamento;
	private String numeroDocumento;
	private java.util.Date dataPagamento;
}
