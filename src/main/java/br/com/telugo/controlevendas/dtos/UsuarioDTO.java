package br.com.telugo.controlevendas.dtos;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import br.com.telugo.controlevendas.enums.TipoUsuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioDTO {
	private long id;	
	private String nome;
	private String cpf;
	private String email;
	private String telefone;
	private TipoUsuario tipo;
	private boolean seParceiroF5;
	private String codigoConsigna;
	private String filial;
	private String cidade;
	private String estado;
}
