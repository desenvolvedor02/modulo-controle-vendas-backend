package br.com.telugo.controlevendas.dtos;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CriarValidacaoVendaDTO {			
	private String nomeBanco;
	private String numeroDocumento;
	private Float valorPagamento;
	private String nomePagador;
	private java.util.Date dataPagamento;
}
