package br.com.telugo.controlevendas.services;

import java.util.Date;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.telugo.controlevendas.dtos.CriarLoginDTO;
import br.com.telugo.controlevendas.enums.StatusLogin;
import br.com.telugo.controlevendas.enums.TipoLogin;
import br.com.telugo.controlevendas.enums.TipoUsuario;
import br.com.telugo.controlevendas.models.Login;
import br.com.telugo.controlevendas.models.Produto;
import br.com.telugo.controlevendas.models.Usuario;
import br.com.telugo.controlevendas.repositories.LoginRepository;
import br.com.telugo.controlevendas.repositories.ProdutoRepository;
import br.com.telugo.controlevendas.repositories.UsuarioRepository;
import javassist.NotFoundException;

@Service
public class LoginService {
		//private LoginMapper loginMapper;
		
		
		private LoginRepository loginRepository;
		private UsuarioRepository usuarioRepository;
		private ProdutoRepository produtoRepository;
		
		public LoginService(
				
				LoginRepository loginRepository,
				UsuarioRepository usuarioRepository,
				ProdutoRepository produtoRepository
				){
			
			this.loginRepository = loginRepository;
			this.usuarioRepository = usuarioRepository;
			this.produtoRepository = produtoRepository;
			}
		//TODO serviço para criar login
		public Login criarLogin(CriarLoginDTO criarLoginDTO) {
			//TODO verificar se existe e tal
			Login login = new Login();
			
			Optional<Usuario> usuarioOptional = usuarioRepository.findById(criarLoginDTO.getIdUsuario());
			Optional<Produto> produtoOptional = produtoRepository.findById(criarLoginDTO.getIdProduto());
			if(usuarioOptional.isPresent()) {
				if(produtoOptional.isPresent()) {
					
					login.setNomeUsuario(criarLoginDTO.getNomeUsuario());
					login.setStatus(StatusLogin.ATIVO);
					login.setTipo(criarLoginDTO.getTipo());
					login.setDataCriacao(criarLoginDTO.getDataCriacao());
					login.setDataValidade(criarLoginDTO.getDataValidade());
					login.setUsuario(usuarioOptional.get());
					login.setProduto(produtoOptional.get());
					loginRepository.save(login);
				}else {
					String msg = "Produto não encontrado para o id passado.";
					throw new ResponseStatusException(
						           HttpStatus.NOT_FOUND, msg, new NotFoundException(msg));
				}
			}else {
				String msg = "Usuario não encontrado para o id passado.";
				throw new ResponseStatusException(
					           HttpStatus.NOT_FOUND, msg, new NotFoundException(msg));
			}
			
			return login;
		}
	
		//Serviço para editar login
		public Login editarLogin(CriarLoginDTO criarLoginDTO, long idLogin) {
			
			Optional<Login> loginOptional = loginRepository.findById(idLogin);
			if(loginOptional.isPresent()) {
				Login login = new Login();
				Optional<Usuario> usuarioOptional = usuarioRepository.findById(criarLoginDTO.getIdUsuario());
				Optional<Produto> produtoOptional = produtoRepository.findById(criarLoginDTO.getIdProduto());
				if(usuarioOptional.isPresent()) {
					if(produtoOptional.isPresent()) {
						login.setNomeUsuario(criarLoginDTO.getNomeUsuario());
						login.setStatus(StatusLogin.ATIVO);
						login.setTipo(criarLoginDTO.getTipo());
						login.setDataCriacao(criarLoginDTO.getDataCriacao());
						login.setDataValidade(criarLoginDTO.getDataValidade());
						login.setUsuario(usuarioOptional.get());
						login.setProduto(produtoOptional.get());
						login.setId(idLogin);
						loginRepository.save(login);
					}else {
						String msg = "Produto não encontrado para o id passado.";
						throw new ResponseStatusException(
							           HttpStatus.NOT_FOUND, msg, new NotFoundException(msg));
					}
				}else {
					String msg = "Usuario não encontrado para o id passado.";
					throw new ResponseStatusException(
						           HttpStatus.NOT_FOUND, msg, new NotFoundException(msg));
				}				
				
				return login;
			}else {
				String msg = "Login não encontrado para o id passado.";
				throw new ResponseStatusException(
					           HttpStatus.NOT_FOUND, msg, new NotFoundException(msg));
			}		
			
		}
		
		
		
		//TODO serviço para buscar logins (retornando paginada) (buscar por filtros)
		public Page<Login> buscarLogins(Pageable pageable, Long id, Long idUsuario,
				Long idProduto, String nomeUsuario,TipoLogin tipo,  Date dataCriacaoInicio, Date dataCriacaoFim,
				Date dataValidadeInicio, Date dataValidadeFim,
				StatusLogin status){
			Usuario usuario = null;
			if(idUsuario!=null) {
				Optional<Usuario> usuarioOptional = usuarioRepository.findById(idUsuario);
				if(usuarioOptional.isPresent()) {
					usuario = usuarioOptional.get();
				}				
			}
			
			Produto produto = null;
			if(idProduto!=null) {
				Optional<Produto> produtoOptional = produtoRepository.findById(idProduto);				
				if(produtoOptional.isPresent()) {
					produto = produtoOptional.get();
				}	
			}
			
			
			Page<Login> loginsPage = loginRepository.buscarLoginsPorFiltros(pageable, id, usuario,
					produto, nomeUsuario,tipo,  dataCriacaoInicio,  dataCriacaoFim,
					dataValidadeInicio, dataValidadeFim,
					status);
			return loginsPage;
		}
		
		//Serviço para mudar status de login
		public Login editarStatusLogin(StatusLogin status, long idLogin) {
			Optional<Login> loginOptional = loginRepository.findById(idLogin);
			if(loginOptional.isPresent()) {
				Login login = loginOptional.get();
				login.setStatus(status);
				login = loginRepository.save(login);
				return login;
			}else {
				String msg = "Login não encontrado para o id passado.";
				throw new ResponseStatusException(
					           HttpStatus.NOT_FOUND, msg, new NotFoundException(msg));
			}	
		}
		
}
