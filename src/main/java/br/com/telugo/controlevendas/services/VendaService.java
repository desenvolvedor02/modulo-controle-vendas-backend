package br.com.telugo.controlevendas.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import br.com.telugo.controlevendas.dtos.CriarLoginDTO;
import br.com.telugo.controlevendas.dtos.CriarVendaDTO;
import br.com.telugo.controlevendas.dtos.RetornoImportacaoDTO;
import br.com.telugo.controlevendas.dtos.ValidacaoVendaERRODTO;
import br.com.telugo.controlevendas.dtos.VendaDTO;
import br.com.telugo.controlevendas.enums.CamposPlanilhaValidarVenda;
import br.com.telugo.controlevendas.enums.FormaPagamento;
import br.com.telugo.controlevendas.enums.StatusLogin;
import br.com.telugo.controlevendas.enums.StatusVenda;
import br.com.telugo.controlevendas.enums.TipoLogin;
import br.com.telugo.controlevendas.mappers.CriarVendaMapper;
import br.com.telugo.controlevendas.models.Login;
import br.com.telugo.controlevendas.models.Produto;
import br.com.telugo.controlevendas.models.Usuario;
import br.com.telugo.controlevendas.models.ValidacaoVenda;
import br.com.telugo.controlevendas.models.Venda;
import br.com.telugo.controlevendas.repositories.LoginRepository;
import br.com.telugo.controlevendas.repositories.ValidacaoVendaRepository;
import br.com.telugo.controlevendas.repositories.VendaRepository;
import javassist.NotFoundException;



@Service
public class VendaService {
	
	private LoginRepository loginRepository;
	//private UsuarioRepository usuarioRepository;
	private VendaRepository vendaRepository;
	private ValidacaoVendaRepository validacaoVendaRepository;
	private CriarVendaMapper criarVendaMapper;
	private LoginService loginService;
	public VendaService(
			
		LoginRepository loginRepository,
		LoginService loginService,
		//UsuarioRepository usuarioRepository,
		VendaRepository vendaRepository,
		ValidacaoVendaRepository validacaoVendaRepository,
		CriarVendaMapper criarVendaMapper
		){
		
		this.loginRepository = loginRepository;
		this.loginService = loginService;
		//this.usuarioRepository = usuarioRepository;
		this.vendaRepository = vendaRepository;
		this.validacaoVendaRepository = validacaoVendaRepository;
		this.criarVendaMapper = criarVendaMapper;
		}
	
	//Serviço para criar venda
	public Venda criarVenda(CriarVendaDTO criarVendaDTO) {
		
		Venda venda = new Venda();
		venda = criarVendaMapper.toEntity(criarVendaDTO);
		//refatorar isso para uma só chamada ao repositorio
		List<Login> logins = new ArrayList<Login>();
		
		
		//Conferir logins para renovaçao
		for(Long idLogin : criarVendaDTO.getRenovacaoLoginsId()) {
			Optional<Login> loginOpt = loginRepository.findById(idLogin);
			if(loginOpt.isPresent()) {
				logins.add(loginOpt.get());
			}
		}	
		venda.setLogins(logins);
		
		//Criando logins novos
		for(CriarLoginDTO loginDTO : criarVendaDTO.getNovosLogins()) {
			Login login = loginService.criarLogin(loginDTO);
			venda.getLogins().add(login);
		}
		venda.setStatus(StatusVenda.AGUARDANDO_CONFIRMACAO);
		venda = vendaRepository.save(venda);		
		return venda;
	}
	
	//Serviço para editar venda
	public Venda editarVenda(CriarVendaDTO criarVendaDTO, Long idVenda) {
		
		Venda venda = new Venda();
		Optional<Venda> vendaOpt = vendaRepository.findById(idVenda);
		if(vendaOpt.isPresent()) {
			venda = vendaOpt.get();
			venda = criarVendaMapper.toEntity(criarVendaDTO);
			//Refatorar isso para uma só chamada ao repositorio
			for(Long idLogin : criarVendaDTO.getRenovacaoLoginsId()) {
				Optional<Login> loginOpt = loginRepository.findById(idLogin);
				if(loginOpt.isPresent()) {
					venda.getLogins().add(loginOpt.get());
				}
			}	
			venda.setId(idVenda);
			venda = vendaRepository.save(venda);	
		}else {
			String msg = "Venda não encontrada para o id passado.";
			throw new ResponseStatusException(
				           HttpStatus.NOT_FOUND, msg, new NotFoundException(msg));
		}
		
		return venda;
	}
	
	//Serviço para buscar vendas (retornando paginada) (buscar por filtros)
	public Page<Venda> buscarVendas(Pageable pageable, Long id, Long idParceiro,
			String favorecido, FormaPagamento formaPagamento,
			Float valor,  String conta, String agencia,
			String operacao, List<Login> logins, String observacao, String numeroDocumento
			, Date dataPagamentoInicio, Date dataPagamentoFim){	
		
		Page<Venda> vendasPage = vendaRepository.buscarVendasPorFiltros(pageable, id, idParceiro,
				favorecido, formaPagamento,
				 valor,  conta, agencia,
				operacao, logins, observacao, numeroDocumento, dataPagamentoInicio, dataPagamentoFim);
		return vendasPage;
	}
	
	//Serviço para mudar status de venda
	public Venda editarStatusVenda(StatusVenda status, Long idVenda) {
		Venda venda = new Venda();
		Optional<Venda> vendaOpt = vendaRepository.findById(idVenda);
		if(vendaOpt.isPresent()) {
			venda = vendaOpt.get();
			return venda;
		}else {
			String msg = "Venda não encontrada para o id passado.";
			throw new ResponseStatusException(
				           HttpStatus.NOT_FOUND, msg, new NotFoundException(msg));
		}
	}
	
	//Escrever csv com vendas passadas
	public StringBuilder escreverArquivo(List<Venda> vendaList, String token) {

	    StringBuilder sb = new StringBuilder();
	    
	    for (Field f : Venda.class.getDeclaredFields()) {
	      switch (f.getName()) {
	        case "idParceiro":
	          sb.append("ID PARCEIRO");
	          break;
	        case "formaPagamento":
	          sb.append("FORMA DE PAGAMENTO");
	          break;
	        case "logins":
	          sb.append("");
	          //sb.append("ID BANCO");
	          break;
	        case "comprovantePagamento":
        	 sb.append("");
	          //sb.append("ID BANCO");
	          break;
	        default:
	          sb.append(f.getName().toUpperCase());
	      }

	      if (!f.getName().equals("logins")
	        && !f.getName().equals("comprovantePagamento")) {
	        sb.append(';');
	      }
	    }
	    
	    sb.deleteCharAt(sb.length() - 1);
	    sb.append('\n');
	    for (Venda venda : vendaList) {
	      for (Field field : Venda.class.getDeclaredFields()) {
	        field.setAccessible(true);
	        try {
	          if (field.get(venda) == null) {
	            sb.append("");
	          } else {

	            if (!field.getName().equals("logins")
	        	    && !field.getName().equals("comprovantePagamento")) {
	              sb.append(field.get(venda).toString());
	            }


	          }

	          if (!field.getName().equals("logins")
		        	    && !field.getName().equals("comprovantePagamento")) {
	            sb.append(';');
	          }
	        } catch (IllegalAccessException e) {
	          e.printStackTrace();
	        }
	      }
	      sb.deleteCharAt(sb.length() - 1);
	      sb.append('\n');
	    }

	    return sb;
	  }
	
	public RetornoImportacaoDTO lerPlanilha(MultipartFile reapExcelDataFile) throws IOException {	
		RetornoImportacaoDTO retornoImportacaoDTO = new RetornoImportacaoDTO();
		List<ValidacaoVenda> validacaoVendaParaSalvarList = new ArrayList<ValidacaoVenda>();
		List<ValidacaoVendaERRODTO> validacaoVendaErroList = new ArrayList<ValidacaoVendaERRODTO>();
		//Abrindo a planilha do arquivo no worksheet 0
		XSSFWorkbook workbook = new XSSFWorkbook(reapExcelDataFile.getInputStream());
	    XSSFSheet worksheet = workbook.getSheetAt(0);
	    
	    //Iterando pelas linhas da planilha
	    for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
	    	XSSFRow row = worksheet.getRow(i);
	    	
	    	//Testando se consegue ler a linha e se ela é branca.
	    	try {
	    		if(testeLinhaBranca(row)) {
	    			 break;
	    		}	        
		      } catch (Exception e) {
		        System.out.println(e.getMessage());
		        break;
		      }
	    	
	    	//VERIFICANDO DADOS OBRIGATORIOS ESTÃO VAZIOS: Caso não estejam lê, caso estejam passa pra prox linha.  	
	    	if(camposObrigatoriosPreenchidos(row)) {
	    		//LENDO OS DADOS
		    	try {
		    		
		    		//RECUPERANDO VALORES DA TABELA
	    			String nomeBanco  = getCellValueAsString(row.getCell(CamposPlanilhaValidarVenda.BANCO.ordinal()));
	    			String numeroDocumento =  getCellValueAsString(row.getCell(CamposPlanilhaValidarVenda.NUMERO_DOCUMENTO.ordinal()));
	    			String nomePagador =  getCellValueAsString(row.getCell(CamposPlanilhaValidarVenda.NOME_PAGADOR.ordinal()));
	    			String valorPagamento =  getCellValueAsString(row.getCell(CamposPlanilhaValidarVenda.VALOR_PAGAMENTO.ordinal()));
	    			String dataPagamento =  getCellValueAsString(row.getCell(CamposPlanilhaValidarVenda.DATA_PAGAMENTO.ordinal()));
	    	 		
	    			//Tratar os dados e criar um obj validacao venda	    			
	    			ValidacaoVenda validacaoVenda = criarValidacaoVendaPorLinha(nomeBanco, numeroDocumento, nomePagador, valorPagamento, dataPagamento);
	    			
	    			//Por a validacaoVenda na lista para salvar
	    			validacaoVendaParaSalvarList.add(validacaoVenda);
	    			
		    	}catch (Exception e) {
	  	          System.out.println("Algo deu errado durante a leitura da planilha. " + e + ": " + e.getMessage());
	  	        }
	    	}else {	    		
	    		ValidacaoVendaERRODTO validacaoVendaErroDTO = new ValidacaoVendaERRODTO();
	    		validacaoVendaErroDTO.setNomeBanco(getCellValueAsString(row.getCell(CamposPlanilhaValidarVenda.BANCO.ordinal())));
	    		validacaoVendaErroDTO.setNumeroDocumento(getCellValueAsString(row.getCell(CamposPlanilhaValidarVenda.NUMERO_DOCUMENTO.ordinal())));
	    		validacaoVendaErroDTO.setNomePagador(getCellValueAsString(row.getCell(CamposPlanilhaValidarVenda.NOME_PAGADOR.ordinal())));
	    		validacaoVendaErroDTO.setValorPagamento(getCellValueAsString(row.getCell(CamposPlanilhaValidarVenda.VALOR_PAGAMENTO.ordinal())));
	    		validacaoVendaErroDTO.setValorPagamento(getCellValueAsString(row.getCell(CamposPlanilhaValidarVenda.DATA_PAGAMENTO.ordinal())));
	    		validacaoVendaErroDTO.setNumeroLinha(i+1);
	    		validacaoVendaErroList.add(validacaoVendaErroDTO);
	    	}    	 
	    	
	    }
	    
	  //salvar os dados apos terem sido lidos.
	    validacaoVendaRepository.saveAll(validacaoVendaParaSalvarList);
	    retornoImportacaoDTO.setValidacaoVendaList(validacaoVendaParaSalvarList);
	    retornoImportacaoDTO.setValidacaoVendaErroDTOList(validacaoVendaErroList);
	    //TODO retornar falhas tbm, e accepted quando n for ok.
	    return retornoImportacaoDTO;
	}
	
	//confirmarPagamentos: Realizar confirmação de pagamento
	public RetornoImportacaoDTO confirmarPagamentos(MultipartFile reapExcelDataFile, Date dataInicio, Date dataFim) throws IOException {
		RetornoImportacaoDTO retornoImportacaoDTO = lerPlanilha(reapExcelDataFile);
		List<String> numerosDocumentoList = retornoImportacaoDTO.getValidacaoVendaList().stream().map(v->v.getNumeroDocumento()).collect(Collectors.toList());
		
		//pegar vendas no range
		//cada uma se tiver uma validacao com o numero dela marque como sim, se não, marque como n.
		List<Venda> vendasNoRange = vendaRepository.buscarVendasPorRangeDatas(dataInicio, dataFim);
		for(Venda venda : vendasNoRange) {
			if(venda.getNumeroDocumento()!=null 
					&& venda.getNumeroDocumento()!=""
					&& venda.getNumeroDocumento()!=" "
					&& numerosDocumentoList.contains(venda.getNumeroDocumento())) {
				venda.setStatus(StatusVenda.PAGAMENTO_CONFIRMADO);
				vendaRepository.save(venda);
			}else {
				venda.setStatus(StatusVenda.PAGAMENTO_NAO_IDENTIFICADO);
				vendaRepository.save(venda);
			}
		}
		retornoImportacaoDTO.setVendasModificadasList(vendasNoRange);
		return retornoImportacaoDTO;
	}
	
	
	
	private boolean testeLinhaBranca(XSSFRow row) {
		boolean linhaEhBranca = false;
		int camposVazios = 0;
		List<CamposPlanilhaValidarVenda> campos = Arrays.asList(CamposPlanilhaValidarVenda.values());
		for(CamposPlanilhaValidarVenda campo : campos ) {	
			if (testeCellVazia(row.getCell(campo.ordinal())) ) {
				camposVazios++;
			 }
		}		
		 if (camposVazios==campos.size() ) {
		   linhaEhBranca = true;
		 } 		 
		 return linhaEhBranca;
	}
	
	private boolean camposObrigatoriosPreenchidos(XSSFRow row) {
		boolean preenchidos = true;
		
		List<CamposPlanilhaValidarVenda> camposObrigatorios = new ArrayList<CamposPlanilhaValidarVenda>();
		camposObrigatorios.add(CamposPlanilhaValidarVenda.BANCO);
		camposObrigatorios.add(CamposPlanilhaValidarVenda.DATA_PAGAMENTO);
		camposObrigatorios.add(CamposPlanilhaValidarVenda.NOME_PAGADOR);
		camposObrigatorios.add(CamposPlanilhaValidarVenda.NUMERO_DOCUMENTO);
		camposObrigatorios.add(CamposPlanilhaValidarVenda.VALOR_PAGAMENTO);
		
		
		for(CamposPlanilhaValidarVenda campo : camposObrigatorios ) {	
			if (testeCellVazia(row.getCell(campo.ordinal())) ) {
				preenchidos = false;
				break;
			 }
		}		 
		
		 return preenchidos;
	}
	
	private boolean testeCellVazia(Cell cell) {
		String valorCell = getCellValueAsString(cell);
		boolean cellVazia = false;
		 if (valorCell == null
		          || valorCell == ""
		          || valorCell == " ") {
			 cellVazia = true;
		 } 
		 return cellVazia;
	}
	
	public String getCellValueAsString(Cell cell) {
	    if (cell == null) {
	        return null;
	    } else if (cell.getCellType() == CellType.STRING) {
	       
	        return cell.toString();
	    } else if (cell.getCellType() == CellType.NUMERIC) {
	        DataFormatter formatter = new DataFormatter();
	        String formattedValue = formatter.formatCellValue(cell);
	        return formattedValue;
	    } else {
	    	String msg = "Valores possiveis são numeros ou strings.";
			throw new ResponseStatusException(
				           HttpStatus.BAD_REQUEST, msg, new NotFoundException(msg));
	    }
	}
	
	private ValidacaoVenda criarValidacaoVendaPorLinha(
			String nomeBanco, String numeroDocumento,
			String nomePagador, String valorPagamento,
			String dataPagamento) throws ParseException {
		ValidacaoVenda validacaoVenda = new ValidacaoVenda();
		validacaoVenda.setNomeBanco(nomeBanco);
		//tratando numero documento:	    			
		numeroDocumento = numeroDocumento.replaceAll("[^0-9]", "");	    		
		validacaoVenda.setNumeroDocumento(numeroDocumento);
		validacaoVenda.setNomePagador(nomePagador);
		//Formatando valor pagamento	    		
		validacaoVenda.setValorPagamento(formatarValorPagamento(valorPagamento));
		
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy"); 
		Date data = formato.parse(dataPagamento);
		validacaoVenda.setDataPagamento(data);
		return validacaoVenda;
	}
	
	
	//Recebe um valor em string e transforma em float.
	private Float formatarValorPagamento(String valorPagamento) {
		//System.out.println("ANTES: "+valorPagamento);
		//Verificar se existe . ou , no valor da celula inserido	    			
		if(valorPagamento.contains(".") || valorPagamento.contains(",")) {
			//Caso exista . ou ,:
				//1) Transforma qualquer , em .
				//2) Separa o valor em duas substrings antes do ultimo 
				//   ponto(o pressuposto ponto decimal) e depois do ultimo ponto.
				//3) Retira caracteres especiais e letras das substrings.
			valorPagamento = valorPagamento.replaceAll(",", ".");	 
			int posPontoCentavos = valorPagamento.lastIndexOf("."); 
			String reais = valorPagamento.substring(0, posPontoCentavos);
			String centavos = valorPagamento.substring(posPontoCentavos, valorPagamento.length());
			String valorFormatado="";
			centavos = centavos.replaceAll("[^0-9]", "");	
			reais = reais.replaceAll("[^0-9]", "");	
			
			//Verificar se a substring centavos tem tamanho 2, o que faz dela os valores da casa decimal.  
			if(centavos.length()==2) {	
				//Caso centavos tem tamanho 2: 
					//real é o valor inteiro e centavos o decimal
					//Formata adequadamenta para virar float:
    				valorFormatado=reais+"."+centavos;
			}else { //centavos é na verdade parte do real.
				//Caso centavos n tem tamanho 2
					//real e centavos são valor inteiro
					//Formata adequadamenta para virar float:
    			valorFormatado=reais+""+centavos+".00";	    			
			}	 
			valorPagamento = valorFormatado;
		}else {
			//Caso nao exista . ou ,:
			//Formata adequadamenta para virar float:
			valorPagamento = valorPagamento.replaceAll("[^0-9]", "");
			valorPagamento+=".00";
		}
		//System.out.println("DEPOIS: "+valorPagamento);
		return Float.valueOf(valorPagamento);
	}
	
	private Object lercell(Cell cell) {
		//FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
		Object valueCell = null;
		if (cell!=null) {
			switch (cell.getCellType()) {
			case BOOLEAN:
			valueCell = cell.getBooleanCellValue();
			System.out.println(cell.getBooleanCellValue());
			System.out.println("b");
			break;
			case NUMERIC:
			valueCell = cell.getNumericCellValue();
			System.out.println(cell.getNumericCellValue());
			System.out.println("n");
			break;
			case STRING:
				
			valueCell = cell.getStringCellValue();
			System.out.println(cell.getStringCellValue());
			System.out.println("s");
			break;
			case BLANK:
			break;
			case ERROR:
			valueCell=cell.getErrorCellValue();
			System.out.println(cell.getErrorCellValue());
			break;
			}			
		}
		return valueCell;
	}
	/*
	public ListaAtuacoesSaldoErroESalvarDTO lerplanilha(MultipartFile reapExcelDataFile, String token) throws IOException {
	    ListaAtuacoesSaldoErroESalvarDTO listaAtuacoesSaldoErroESalvarDTO = new ListaAtuacoesSaldoErroESalvarDTO();
	    List<AtuacaoSaldo> atuacoesSaldosSalvar = new ArrayList<AtuacaoSaldo>();
	    List<AtuacaoSaldo> atuacoesSaldosErro = new ArrayList<AtuacaoSaldo>();
	    String linhasComErro = "";
	    AtuacaoSaldoDadosPlanilhaDTO atuacaoSaldoDadosPlanilhaDTO = new AtuacaoSaldoDadosPlanilhaDTO();
	    XSSFWorkbook workbook = new XSSFWorkbook(reapExcelDataFile.getInputStream());
	    XSSFSheet worksheet = workbook.getSheetAt(0);
	    for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
	      XSSFRow row = worksheet.getRow(i);
	      //Testando se o primeiro elemento da linha está branco. Se tiver, considera a linha como branca e não a lê.
	      try {
	        if (row.getCell(ColunasTabelaAtuacaoSaldo.CLIENTE.ordinal()) == null
	          || row.getCell(ColunasTabelaAtuacaoSaldo.CLIENTE.ordinal()).toString() == ""
	          || row.getCell(ColunasTabelaAtuacaoSaldo.CLIENTE.ordinal()).toString().trim() == " ") {
	          break;
	        } else {
	          System.out.println("TESTE DA LINHA BRANCA");
	          System.out.println(row.getCell(ColunasTabelaAtuacaoSaldo.CLIENTE.ordinal()).toString());
	        }
	      } catch (Exception e) {
	        System.out.println(e.getMessage());
	        break;
	      }

	      String colunasComErro = verificarPlanilhaSeHaDadosObrigatoriosVazios(row);
	      if (colunasComErro.equals("")) {
	        DataFormatter df = new DataFormatter();
	        Parceiro parceiroPromotor = null;
	        AtuacaoSaldo atuacaoSaldo = new AtuacaoSaldo();
	        try {
	          //LENDO OS DADOS
	          atuacaoSaldo.setCliente(row.getCell(ColunasTabelaAtuacaoSaldo.CLIENTE.ordinal()).toString());
	          atuacaoSaldo.setCpf(df.formatCellValue(row.getCell(ColunasTabelaAtuacaoSaldo.CPF.ordinal())));
	          atuacaoSaldo.setProposta(df.formatCellValue(row.getCell(ColunasTabelaAtuacaoSaldo.PROPOSTA.ordinal())));
	          atuacaoSaldo.setSaldoDigitado(convertStringToBigDecimal(row.getCell(ColunasTabelaAtuacaoSaldo.SALDODIGITADO.ordinal()).toString()));
	          atuacaoSaldo.setSaldoRetornado(convertStringToBigDecimal(row.getCell(ColunasTabelaAtuacaoSaldo.SALDORETORNADO.ordinal()).toString()));
	          Double taxaJuros = convertStringToBigDecimal(row.getCell(ColunasTabelaAtuacaoSaldo.TAXAJUROS.ordinal()).toString());
	          if (taxaJuros != null)
	            atuacaoSaldo.setTaxaJuros(taxaJuros / 100);
	          else
	            throw new BadRequestException("Taxa de Juros Inválida.");
	          atuacaoSaldo.setNomeBanco(row.getCell(ColunasTabelaAtuacaoSaldo.BANCO.ordinal()).toString());
	          atuacaoSaldo.setConvenio(row.getCell(ColunasTabelaAtuacaoSaldo.CONVENIO.ordinal()).toString());
	          atuacaoSaldo.setParcelaDigitada(row.getCell(ColunasTabelaAtuacaoSaldo.PARCELADIGITADA.ordinal()).getNumericCellValue());

	          if (row.getCell(ColunasTabelaAtuacaoSaldo.NOVAPARCELA.ordinal()) == null) {
	            atuacaoSaldo.setNovaParcela(null);
	          } else {
	            if (!row.getCell(ColunasTabelaAtuacaoSaldo.NOVAPARCELA.ordinal()).toString().equals("") &&
	              !row.getCell(ColunasTabelaAtuacaoSaldo.NOVAPARCELA.ordinal()).toString().trim().equals(" ")) {
	              atuacaoSaldo.setNovaParcela(row.getCell(ColunasTabelaAtuacaoSaldo.NOVAPARCELA.ordinal()).getNumericCellValue());
	            }
	          }

	          if (row.getCell(ColunasTabelaAtuacaoSaldo.TROCOAPROXIMADO.ordinal()) == null) {
	            atuacaoSaldo.setTrocoAproximado(null);
	          } else {
	            atuacaoSaldo.setTrocoAproximado(convertStringToBigDecimal(row.getCell(ColunasTabelaAtuacaoSaldo.TROCOAPROXIMADO.ordinal()).toString()));
	          }

	          if (row.getCell(ColunasTabelaAtuacaoSaldo.PROMOTOR.ordinal()) == null) {
	            atuacaoSaldo.setUsuarioDigitador("");
	          } else {
	            atuacaoSaldo.setUsuarioDigitador(df.formatCellValue(row.getCell(ColunasTabelaAtuacaoSaldo.PROMOTOR.ordinal())));
	          }
	          try {
	            String idConsignaParceiro = df.formatCellValue(row.getCell(ColunasTabelaAtuacaoSaldo.IDCONSIGNAPARCEIRO.ordinal())).trim();
	            System.out.println(ColunasTabelaAtuacaoSaldo.IDCONSIGNAPARCEIRO.ordinal());
	            if (!idConsignaParceiro.equals("")) {
	              parceiroPromotor = clientsService.obterParceiroPorIdConsigna(token, Long.valueOf(idConsignaParceiro));
	            }
	          } catch (Exception e) {
	            System.out.println("erro " + e);
	          }
	          atuacaoSaldo.setEstadoAtuacaoSaldo(EstadoAtuacaoSaldo.SALDO_AVALIAR);
	          atuacaoSaldo.setStatusBanco(StatusBanco.EM_ANDAMENTO);
	          atuacaoSaldo.setExcluido(false);
	          atuacaoSaldo.setRespondidoPeloNCC(false);
	          atuacaoSaldo.setRespondidoPeloParceiro(false);

	          if (parceiroPromotor == null) {
	            parceiroPromotor = clientsService.retornarParceiroPorUsuarioDigitador(token, atuacaoSaldo.getUsuarioDigitador());
	          }
	          if (parceiroPromotor != null) {
	            atuacaoSaldo.setIdParceiro(parceiroPromotor.getId());
	            atuacaoSaldo.setIdParceiroConsigna(parceiroPromotor.getIdConsigna());
	            atuacaoSaldo.setNomePromotor(parceiroPromotor.getNome());
	            atuacaoSaldo.setIdPessoa(parceiroPromotor.getIdPessoa());
	            atuacaoSaldo.setIdFilial(parceiroPromotor.getIdFilial());
	            System.out.println(parceiroPromotor.getIdFilial());
	            atuacaoSaldo.setNomeFilial(clientsService.obterFilialPorIdConsigna(token, atuacaoSaldo.getIdFilial().longValue()).getNome());
	          }
	          atuacaoSaldo.setObservacaoAprovacao(null);
	        } catch (Exception e) {
	          System.out.println("Algo deu errado durante a importação de uma atuação saldo. " + e + ": " + e.getMessage());
	        }

	        //Só vai ser adicionado caso promotor não seja nulo.
	        if (parceiroPromotor != null) {
	          List<AtuacaoSaldo> atuacaoSaldoRetornadaList = atuacaoSaldoRepository.findByProposta(atuacaoSaldo.getProposta());
	          if (atuacaoSaldoRetornadaList.size() == 0) {
	            atuacoesSaldosSalvar.add(atuacaoSaldo);
	          } else {
	            System.out.println("Algo deu errado durante a importação de uma atuação saldo. AtuacaoSaldo com essa proposta já existe sem estar expirada.");
	            atuacoesSaldosErro.add(atuacaoSaldo);
	            linhasComErro += (i + 1) + ", ";
	          }
	        } else {
	          System.out.println("Algo deu errado durante a importação de uma atuação saldo. Não foi passado Parceiro id consigna");
	          atuacoesSaldosErro.add(atuacaoSaldo);
	          linhasComErro += (i + 1) + ", ";
	        }
	      } else {
	        //Se algum dado obrigatório for vazio
	        listaAtuacoesSaldoErroESalvarDTO.setColunasComErro(colunasComErro);
	      }
	    }

	    listaAtuacoesSaldoErroESalvarDTO.setAtuacoesSaldosErro(atuacoesSaldosErro);
	    listaAtuacoesSaldoErroESalvarDTO.setAtuacoesSaldosSalvar(atuacoesSaldosSalvar);
	    listaAtuacoesSaldoErroESalvarDTO.setLinhasComErro(linhasComErro);
	    return listaAtuacoesSaldoErroESalvarDTO;
	  }*/
		
}
