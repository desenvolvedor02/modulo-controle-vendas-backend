package br.com.telugo.controlevendas.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.telugo.controlevendas.dtos.CriarUsuarioDTO;
import br.com.telugo.controlevendas.enums.TipoUsuario;
import br.com.telugo.controlevendas.mappers.CriarUsuarioMapper;
import br.com.telugo.controlevendas.models.Produto;
import br.com.telugo.controlevendas.models.Usuario;
import br.com.telugo.controlevendas.repositories.UsuarioRepository;
import javassist.NotFoundException;

@Service
public class UsuarioService {
	
	private CriarUsuarioMapper criarUsuarioMapper;
	private UsuarioRepository usuarioRepository;
	
	public UsuarioService(
			CriarUsuarioMapper criarUsuarioMapper,
			UsuarioRepository usuarioRepository
			){
		this.criarUsuarioMapper = criarUsuarioMapper;
		this.usuarioRepository = usuarioRepository;
		}
		
		//TODO serviço para criar usuario
		public Usuario criarUsuario(CriarUsuarioDTO criarUsuarioDTO) {
			//TODO verificar se existe e tal
			Optional<Usuario> usuarioOpt = usuarioRepository.findByNome(criarUsuarioDTO.getNome());
			if(!usuarioOpt.isPresent()) {
			Usuario usuario = criarUsuarioMapper.toEntity(criarUsuarioDTO);
			usuarioRepository.save(usuario);
			return usuario;
			}else {
				String msg = "Usuário já existe com o nome passado.";
				throw new ResponseStatusException(
					           HttpStatus.BAD_REQUEST, msg, new NotFoundException(msg));
			}
		}
	
		//Serviço para editar usuario
		public Usuario editarUsuario(CriarUsuarioDTO criarUsuarioDTO, long idUsuario) {
			Optional<Usuario> usuarioOptional = usuarioRepository.findById(idUsuario);
			if(usuarioOptional.isPresent()) {
				Usuario usuario = criarUsuarioMapper.toEntity(criarUsuarioDTO);
				usuario.setId(idUsuario);
				usuarioRepository.save(usuario);
				return usuario;
			}else {
				String msg = "Usuario não encontrado para o id passado.";
				throw new ResponseStatusException(
					           HttpStatus.NOT_FOUND, msg, new NotFoundException(msg));
			}		
			
		}
		
		//Serviço para buscar usuarios (retornando paginada) (buscar por filtros, mande o filtro null caso queira desconsidera-lo na busca)
		public Page<Usuario> buscarUsuarios(Pageable pageable, Long id, String nome, String cpf, String email,
				String telefone, TipoUsuario tipo, Boolean seParceiroF5, String codigoConsigna, String filial,
				String cidade, String estado) {
			Page<Usuario> usuariosPage = usuarioRepository.buscarUsuariosPorFiltros(pageable, id, nome, cpf, email,
					telefone, tipo, seParceiroF5, codigoConsigna, filial,
					cidade, estado);
			return usuariosPage;
		}
		
}
