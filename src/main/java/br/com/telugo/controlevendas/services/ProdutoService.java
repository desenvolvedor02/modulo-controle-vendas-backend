package br.com.telugo.controlevendas.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.telugo.controlevendas.dtos.CriarLoginDTO;
import br.com.telugo.controlevendas.dtos.CriarProdutoDTO;
import br.com.telugo.controlevendas.mappers.CriarProdutoMapper;
import br.com.telugo.controlevendas.models.Produto;
import br.com.telugo.controlevendas.repositories.ProdutoRepository;
import javassist.NotFoundException;

@Service
public class ProdutoService {
	private CriarProdutoMapper criarProdutoMapper;
	private ProdutoRepository produtoRepository;
	public ProdutoService(
			CriarProdutoMapper criarProdutoMapper,
			ProdutoRepository produtoRepository
			){
		this.criarProdutoMapper = criarProdutoMapper;
		this.produtoRepository = produtoRepository;
		}
		
		//TODO serviço para criar produto
		public Produto criarProduto(CriarProdutoDTO criarProdutoDTO) {
			//TODO verificar se existe e tal
			Optional<Produto> produtoOpt = produtoRepository.findByNome(criarProdutoDTO.getNome());
			if(!produtoOpt.isPresent()) {
				Produto produto = criarProdutoMapper.toEntity(criarProdutoDTO);
				produtoRepository.save(produto);
				return produto;
			}else {
				String msg = "Produto já existe com o nome passado.";
				throw new ResponseStatusException(
					           HttpStatus.BAD_REQUEST, msg, new NotFoundException(msg));
			}			
			
		}
	
		//Serviço para editar produto
		public Produto editarProduto(CriarProdutoDTO criarProdutoDTO, long idProduto) {
			Optional<Produto> produtoOptional = produtoRepository.findById(idProduto);
			if(produtoOptional.isPresent()) {
				Produto produto = criarProdutoMapper.toEntity(criarProdutoDTO);
				produto.setId(idProduto);
				produtoRepository.save(produto);
				return produto;
			}else {
				String msg = "Produto não encontrado para o id passado.";
				throw new ResponseStatusException(
					           HttpStatus.NOT_FOUND, msg, new NotFoundException(msg));
			}		
			
		}
		
		//Serviço para buscar produtos (retornando paginada) (buscar por filtros, mande o filtro null caso queira desconsidera-lo na busca)
		public Page<Produto> buscarProdutos(Pageable pageable, Long id, String nome, Float valorCompra, Float valorVenda, Integer duracaoEmDias, String fornecedor, Float margemDeGanho) {
			Page<Produto> produtosPage = produtoRepository.buscarProdutosPorFiltros(pageable, id, nome, valorCompra, valorVenda, duracaoEmDias, fornecedor, margemDeGanho);
			return produtosPage;
		}
		
}
