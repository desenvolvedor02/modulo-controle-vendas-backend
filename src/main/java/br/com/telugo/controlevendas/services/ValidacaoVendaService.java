package br.com.telugo.controlevendas.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.telugo.controlevendas.dtos.CriarUsuarioDTO;
import br.com.telugo.controlevendas.dtos.CriarValidacaoVendaDTO;
import br.com.telugo.controlevendas.enums.TipoUsuario;
import br.com.telugo.controlevendas.mappers.CriarUsuarioMapper;
import br.com.telugo.controlevendas.models.Produto;
import br.com.telugo.controlevendas.models.Usuario;
import br.com.telugo.controlevendas.models.ValidacaoVenda;
import br.com.telugo.controlevendas.repositories.UsuarioRepository;
import br.com.telugo.controlevendas.repositories.ValidacaoVendaRepository;
import javassist.NotFoundException;

@Service
public class ValidacaoVendaService {
	
	//private CriarUsuarioMapper criarUsuarioMapper;
	private ValidacaoVendaRepository validacaoVendaRepository;
	
	public ValidacaoVendaService(
			//CriarUsuarioMapper criarUsuarioMapper,
			ValidacaoVendaRepository validacaoVendaRepository
			){
		//this.criarUsuarioMapper = criarUsuarioMapper;
		this.validacaoVendaRepository = validacaoVendaRepository;
		}
		
		//TODO serviço para criar validacao venda
		public ValidacaoVenda criarValidacaoVenda(CriarValidacaoVendaDTO criarValidacaoVendaDTO) {			
			ValidacaoVenda validacao = new ValidacaoVenda();
			validacao.setNomeBanco(criarValidacaoVendaDTO.getNomeBanco());
			validacao.setNumeroDocumento(criarValidacaoVendaDTO.getNumeroDocumento());
			validacao.setValorPagamento(criarValidacaoVendaDTO.getValorPagamento());
			validacao.setDataPagamento(criarValidacaoVendaDTO.getDataPagamento());
			validacao = validacaoVendaRepository.save(validacao);
			return validacao;
		}	
		
		//Serviço para criar lista de validacao venda
		public List<ValidacaoVenda> criarValidacaoVendaList(List<CriarValidacaoVendaDTO> criarValidacaoVendaDTOList) {			
			List<ValidacaoVenda> validacaoList = new ArrayList<ValidacaoVenda>();
			for(CriarValidacaoVendaDTO dto : criarValidacaoVendaDTOList) {
				ValidacaoVenda validacao = new ValidacaoVenda();
				validacao.setNomeBanco(dto.getNomeBanco());
				validacao.setNumeroDocumento(dto.getNumeroDocumento());
				validacao.setValorPagamento(dto.getValorPagamento());
				validacao.setDataPagamento(dto.getDataPagamento());
				validacaoList.add(validacao);
				
			}
			validacaoList = validacaoVendaRepository.saveAll(validacaoList);
			return validacaoList;
		}	
		
		
		//TODO Serviço para buscar validacao vendas (retornando paginada) (buscar por filtros, mande o filtro null caso queira desconsidera-lo na busca)
		public Page<ValidacaoVenda> buscarValicacaoVendas(Pageable pageable, Long id, String nomeBanco,
		String numeroDocumento, String nomePagador,
		Float valorPagamento,  Date dataPagamentoInicio, Date dataPagamentoFim) {
			Page<ValidacaoVenda> validacaoVendaPage = validacaoVendaRepository.buscarValidacaoVendasPorFiltros(pageable, id, nomeBanco,
					numeroDocumento, nomePagador, valorPagamento,  dataPagamentoInicio, dataPagamentoFim);
			return validacaoVendaPage;
		}
		
}
