package br.com.telugo.controlevendas.enums;

public enum StatusLogin {
ATIVO,
INATIVO,
RENOVADO,
CANCELADO,
EM_ANALISE
}
