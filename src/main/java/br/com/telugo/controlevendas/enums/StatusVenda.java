package br.com.telugo.controlevendas.enums;

public enum StatusVenda {
PAGAMENTO_CONFIRMADO,
AGUARDANDO_CONFIRMACAO,
PAGAMENTO_NAO_IDENTIFICADO
}
