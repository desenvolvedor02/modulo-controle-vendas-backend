package br.com.telugo.controlevendas.enums;

public enum CamposPlanilhaValidarVenda {
BANCO,
NUMERO_DOCUMENTO,
NOME_PAGADOR,
VALOR_PAGAMENTO,
DATA_PAGAMENTO
}
