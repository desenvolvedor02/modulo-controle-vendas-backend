package br.com.telugo.controlevendas.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import br.com.telugo.controlevendas.dtos.CriarUsuarioDTO;
import br.com.telugo.controlevendas.dtos.CriarVendaDTO;
import br.com.telugo.controlevendas.dtos.RetornoImportacaoDTO;
import br.com.telugo.controlevendas.dtos.UsuarioDTO;
import br.com.telugo.controlevendas.dtos.VendaDTO;
import br.com.telugo.controlevendas.enums.FormaPagamento;
import br.com.telugo.controlevendas.enums.StatusVenda;
import br.com.telugo.controlevendas.enums.TipoUsuario;
import br.com.telugo.controlevendas.mappers.UsuarioMapper;
import br.com.telugo.controlevendas.mappers.VendaMapper;
import br.com.telugo.controlevendas.models.Login;
import br.com.telugo.controlevendas.models.Usuario;
import br.com.telugo.controlevendas.models.ValidacaoVenda;
import br.com.telugo.controlevendas.models.Venda;
import br.com.telugo.controlevendas.services.UsuarioService;
import br.com.telugo.controlevendas.services.VendaService;
import br.com.usuario.AutenticadorUsuario;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping(value="/api/vendas")
@Api(value="API REST PARA VENDAS")
@CrossOrigin(origins = "*", maxAge = 3600)
public class VendaController  implements AutenticadorUsuario{
	
	//Dependências
	private VendaService vendaService;
	private VendaMapper vendaMapper;
	public VendaController(VendaService vendaService, VendaMapper vendaMapper) {
		this.vendaService = vendaService;
		this.vendaMapper = vendaMapper;
	}
	
	/*
	 * ENDPOINT PARA CRIAR VENDA
	 * 
	 *@param CriarVendaDTO
	 *@return ResponseEntity<VendaDTO>
	 * */
	@PostMapping("/criar")
    @ApiOperation(value="ENDPOINT PARA CRIAR VENDA", response=VendaDTO.class)
    public ResponseEntity<VendaDTO> criarVenda(
    		@RequestHeader(value = "Authorization") String token,
    		@RequestBody CriarVendaDTO criarVendaDTO) {
		this.autenticacaoTokenTryCatch(token);
		Venda venda = vendaService.criarVenda(criarVendaDTO); 
	   return ResponseEntity.ok(vendaMapper.toDto(venda));
	}  
	
	/*
	 * ENDPOINT PARA EDITAR VENDA
	 * 
	 *@param CriarVendaDTO
	 *@param long idVenda
	 *@return ResponseEntity<VendaDTO>
	 * */
	@PostMapping("/editar/{idVenda}")
    @ApiOperation(value="ENDPOINT PARA EDITAR VENDA", response=VendaDTO.class)
    public ResponseEntity<VendaDTO> editarVenda(
    		@RequestHeader(value = "Authorization") String token,
    		@RequestBody CriarVendaDTO criarVendaDTO, @PathVariable Long idVenda) {
		
		this.autenticacaoTokenTryCatch(token);
		
		Venda venda = vendaService.editarVenda(criarVendaDTO, idVenda);
	   return ResponseEntity.ok(vendaMapper.toDto(venda));
	}
	
	/*
	 * ENDPOINT PARA MUDAR STATUS DE VENDA
	 * 
	 *@param StatusVenda
	 *@param long idVenda
	 *@return ResponseEntity<VendaDTO>
	 * */
	@PostMapping("/editar-status/{idVenda}")
    @ApiOperation(value="ENDPOINT PARA MUDAR STATUS DE VENDA", response=VendaDTO.class)
    public ResponseEntity<VendaDTO> editarStatusVenda(
    		@RequestHeader(value = "Authorization") String token,
    		@RequestBody StatusVenda status, @PathVariable Long idVenda) {
		
		this.autenticacaoTokenTryCatch(token);
		
		Venda venda = vendaService.editarStatusVenda(status, idVenda);
	   return ResponseEntity.ok(vendaMapper.toDto(venda));
	}
	
	
	/*
	 * ENDPOINT PARA LISTAR VENDAS DE ACORDO COM FILTROS PASSADOS (ENVIE NULL CASO QUEIRA DESCONSIDERAR)
	 * 
	 *
	 *
	 *@return ResponseEntity<List<UsuarioDTO>>
	 * */
	@GetMapping("/list")
	@ApiOperation(value="ENDPOINT PARA LISTAR PRODUTOS DE ACORDO COM FILTROS PASSADOS (ENVIE NULL CASO QUEIRA DESCONSIDERAR)",
	response=VendaDTO[].class)
	@ApiImplicitParams({
		@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
		@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page.", defaultValue = "5"),
		@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.") 
		})
	public ResponseEntity<List<VendaDTO>> listVendas(
			@ApiIgnore("Ignored because swagger ui shows the wrong params, instead they are explained in the implicit params")
			Pageable pageable,
			@RequestHeader(value = "Authorization") String token,
			@RequestParam(required=false) Long id,
			@RequestParam(required=false) Long idParceiro,
			@RequestParam(required=false) String favorecido,
			@RequestParam(required=false) FormaPagamento formaPagamento,
			@RequestParam(required=false) Float valor,
			@RequestParam(required=false) String conta,
			@RequestParam(required=false) String agencia,
			@RequestParam(required=false) String operacao,
			@RequestParam(required=false) List<Login> logins,
			@RequestParam(required=false) String observacao,
			@RequestParam(required=false) String numeroDocumento,
			@RequestParam(required=false) Date dataPagamentoInicio,
			@RequestParam(required=false) Date dataPagamentoFim){
		
		this.autenticacaoTokenTryCatch(token);
		
		Page<Venda> vendasPage = vendaService.buscarVendas(pageable, id, idParceiro, favorecido, formaPagamento,
				valor, conta, agencia, operacao, logins, observacao, numeroDocumento,
				dataPagamentoInicio, dataPagamentoFim);
		//Coloca total de paginas no header
				int totalPages = vendasPage.getTotalPages();		
				long totalElements = vendasPage.getTotalElements();
				HttpHeaders responseHeaders = new HttpHeaders();
				responseHeaders.set("totalPages",
				Integer.toString(totalPages));
				responseHeaders.set("totalElements",
						Long.toString(totalElements));
				responseHeaders.set("Access-Control-Expose-Headers", "totalPages");
				responseHeaders.set("Access-Control-Expose-Headers", "totalElements");
				return ResponseEntity.ok().headers(responseHeaders).body(vendaMapper.toDto(vendasPage.getContent()));
	}
	
	
	/*
	 * ENDPOINT PARA EXPORTAR PLANILHA DE VENDAS EM CSV
	 * 
	 *@param 
	 *@param 
	 *@return ResponseEntity<VendaDTO>
	 * */
	//@GetMapping(value  = "/create-file", produces = "text/csv")
	@PostMapping(path = "/export")
	@ApiOperation(value = "ENDPOINT PARA EXPORTAR PLANILHA DE VENDAS EM CSV.", response = String.class)
	  public ResponseEntity<String> exportCSV(HttpServletResponse response,
	                                          @RequestHeader(value = "Authorization") String token,
	                                          @RequestBody List<Venda> vendas) throws Exception {

	    this.autenticacaoTokenTryCatch(token);

	    StringBuilder retornoArquivo = vendaService.escreverArquivo(vendas, null);

	    String csvFileName = "vendas.csv";
	    response.setContentType("text/csv; charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
	    String headerKey = "Content-Disposition";
	    String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
	    //response.setHeader("Content-encoding","UTF-8");
	    response.setHeader(headerKey, headerValue);
	    response.getWriter().write(String.valueOf(retornoArquivo));
	    response.getWriter().close();

	    return ResponseEntity.ok("");
	  }
	
	
	
	@PostMapping(path = "/importar-planilha-validar-vendas")
	 @ApiOperation(value = "ENDPOINT PARA VALIDAR VENDAS IMPORTANDO PLANILHA E VALIDAR VENDAS", response = Venda[].class)
	  public ResponseEntity<List<Venda>> importar(
	    @RequestHeader(value = "Authorization") String token,
	    @RequestParam("file") MultipartFile reapExcelDataFile,
	    @RequestParam Date dataInicio,
	    @RequestParam Date dataFim
	  ) throws IOException {
		
	    this.autenticacaoTokenTryCatch(token);
		RetornoImportacaoDTO retornoImportacaoDTO  = vendaService.confirmarPagamentos(reapExcelDataFile, dataInicio, dataFim);
		if (retornoImportacaoDTO.getValidacaoVendaErroDTOList().size() > 0 ||retornoImportacaoDTO.getValidacaoVendaList().size()!=retornoImportacaoDTO.getVendasModificadasList().size() ) {
		
			String linhasComErro = "As seguintes linhas apresentaram erros na importação: ";
			List<Integer> numerosLinhasErro = retornoImportacaoDTO.getValidacaoVendaErroDTOList().stream().map(erro-> erro.getNumeroLinha()).collect(Collectors.toList());
			for(Integer n: numerosLinhasErro) {
				linhasComErro+=n+", ";
			}
			 HttpHeaders responseHeaders = new HttpHeaders();
		      responseHeaders.set("linhascomerro", linhasComErro);
		      responseHeaders.set("Access-Control-Expose-Headers", "linhascomerro");
		      return ResponseEntity.status(HttpStatus.ACCEPTED).headers(responseHeaders).body(retornoImportacaoDTO.getVendasModificadasList());
		
		}else {
			return ResponseEntity.status(HttpStatus.OK).body(retornoImportacaoDTO.getVendasModificadasList());
		}	  
	  }
	
	@PostMapping(path = "/importar-planilha")
	 @ApiOperation(value = "ENDPOINT PARA VALIDAR VENDAS IMPORTANDO PLANILHA", response = String.class)
	  public ResponseEntity<String> importarEValidar(
	    @RequestHeader(value = "Authorization") String token,
	    @RequestParam("file") MultipartFile reapExcelDataFile
	  ) throws IOException {
		
	    this.autenticacaoTokenTryCatch(token);
	    vendaService.lerPlanilha(reapExcelDataFile);
	    //ListaAtuacoesSaldoErroESalvarDTO listaAtuacoesSaldoErroESalvarDTO = atuacaoSaldoService.importaresalvar(reapExcelDataFile, token);
	    //List<AtuacaoSaldoDTO> atuacoessaldosDTO = atuacaoSaldoMapper.toDto(listaAtuacoesSaldoErroESalvarDTO.getAtuacoesSaldosSalvar());

	    /*if (listaAtuacoesSaldoErroESalvarDTO.getAtuacoesSaldosErro().size() > 0) {
	      String linhasComErro = "As seguintes linhas apresentaram erros na importação: ";
	      linhasComErro += listaAtuacoesSaldoErroESalvarDTO.getLinhasComErro();
	      linhasComErro = atuacaoSaldoService.replaceLast(linhasComErro, ",", ".");
	      HttpHeaders responseHeaders = new HttpHeaders();
	      responseHeaders.set("linhascomerro", linhasComErro);
	      responseHeaders.set("Access-Control-Expose-Headers", "linhascomerro");
	      return ResponseEntity.status(HttpStatus.ACCEPTED).headers(responseHeaders).body(atuacoessaldosDTO);
	    } else {
	      return ResponseEntity.status(HttpStatus.OK).body(atuacoessaldosDTO);
	    }*/
	    return ResponseEntity.status(HttpStatus.OK).body("");
	  }
	
	
	public void autenticacaoTokenTryCatch(String token) {
		/*if(token.isEmpty()) {
			String msg = "SEM AUTENTICAÇÃO";
			throw new ResponseStatusException(
				           HttpStatus.UNAUTHORIZED, msg, new NotFoundException(msg));
		}	*/
		try {
            obterUsuario(token);
        } catch (Exception e) {
        	String msg = "Usuario não autorizado";
			throw new ResponseStatusException(
				           HttpStatus.UNAUTHORIZED, msg, new NotFoundException(msg));
        }
	}
	
}
