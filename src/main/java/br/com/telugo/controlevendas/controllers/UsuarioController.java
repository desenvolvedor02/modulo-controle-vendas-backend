package br.com.telugo.controlevendas.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.telugo.controlevendas.dtos.CriarUsuarioDTO;
import br.com.telugo.controlevendas.dtos.UsuarioDTO;
import br.com.telugo.controlevendas.enums.TipoUsuario;
import br.com.telugo.controlevendas.mappers.UsuarioMapper;
import br.com.telugo.controlevendas.models.Usuario;
import br.com.telugo.controlevendas.services.UsuarioService;
import br.com.usuario.AutenticadorUsuario;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping(value="/api/usuarios")
@Api(value="API REST PARA USUARIOS")
@CrossOrigin(origins = "*", maxAge = 3600)
public class UsuarioController  implements AutenticadorUsuario{
	
	//Dependências
	private UsuarioService usuarioService;
	private UsuarioMapper usuarioMapper;
	public UsuarioController(UsuarioService usuarioService, UsuarioMapper usuarioMapper) {
		this.usuarioService = usuarioService;
		this.usuarioMapper = usuarioMapper;
	}
	
	/*
	 * ENDPOINT PARA CRIAR USUARIO
	 * 
	 *@param CriarUsuarioDTO
	 *@return ResponseEntity<UsuarioDTO>
	 * */
	@PostMapping("/criar")
    @ApiOperation(value="ENDPOINT PARA CRIAR USUARIO", response=UsuarioDTO.class)
    public ResponseEntity<UsuarioDTO> criarProduto(
    		@RequestHeader(value = "Authorization") String token,
    		@RequestBody CriarUsuarioDTO criarUsuarioDTO) {
		
		this.autenticacaoTokenTryCatch(token);

		Usuario usuario = usuarioService.criarUsuario(criarUsuarioDTO); 
	   return ResponseEntity.ok(usuarioMapper.toDto(usuario));
	}  
	
	/*
	 * ENDPOINT PARA EDITAR USUARIO
	 * 
	 *@param CriarUsuarioDTO
	 *@param long idUsuario
	 *@return ResponseEntity<UsuarioDTO>
	 * */
	@PostMapping("/editar/{idUsuario}")
    @ApiOperation(value="ENDPOINT PARA EDITAR USUARIO", response=UsuarioDTO.class)
    public ResponseEntity<UsuarioDTO> editarUsuario(
    		@RequestHeader(value = "Authorization") String token,
    		@RequestBody CriarUsuarioDTO criarUsuarioDTO, @PathVariable Long idUsuario) {
		
		this.autenticacaoTokenTryCatch(token);

	   Usuario usuario = usuarioService.editarUsuario(criarUsuarioDTO, idUsuario);
	   return ResponseEntity.ok(usuarioMapper.toDto(usuario));
	}
	
	/*
	 * ENDPOINT PARA LISTAR USUARIOS DE ACORDO COM FILTROS PASSADOS (ENVIE NULL CASO QUEIRA DESCONSIDERAR)
	 * 
	 *
	 *
	 *@return ResponseEntity<List<UsuarioDTO>>
	 * */
	@GetMapping("/list")
	@ApiOperation(value="ENDPOINT PARA LISTAR PRODUTOS DE ACORDO COM FILTROS PASSADOS (ENVIE NULL CASO QUEIRA DESCONSIDERAR)",
	response=UsuarioDTO[].class)
	@ApiImplicitParams({
		@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
		@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page.", defaultValue = "5"),
		@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.") 
		})
	public ResponseEntity<List<UsuarioDTO>> listUsuarios(
			@ApiIgnore("Ignored because swagger ui shows the wrong params, instead they are explained in the implicit params")
			Pageable pageable,
			@RequestHeader(value = "Authorization") String token,
			@RequestParam(required=false) Long id,
			@RequestParam(required=false) String nome,
			@RequestParam(required=false) String cpf,
			@RequestParam(required=false) String email,
			@RequestParam(required=false) String telefone,
			@RequestParam(required=false) TipoUsuario tipo,
			@RequestParam(required=false) Boolean seParceiroF5,
			@RequestParam(required=false) String codigoConsigna,
			@RequestParam(required=false) String filial,
			@RequestParam(required=false) String cidade,
			@RequestParam(required=false) String estado){
		
		this.autenticacaoTokenTryCatch(token);

		
		Page<Usuario> usuariosPage = usuarioService.buscarUsuarios(pageable, id, nome, cpf, email,
				telefone, tipo, seParceiroF5, codigoConsigna, filial,
				cidade, estado);
		//Coloca total de paginas no header
				int totalPages = usuariosPage.getTotalPages();		
				long totalElements = usuariosPage.getTotalElements();
				HttpHeaders responseHeaders = new HttpHeaders();
				responseHeaders.set("totalPages",
				Integer.toString(totalPages));
				responseHeaders.set("totalElements",
						Long.toString(totalElements));
				responseHeaders.set("Access-Control-Expose-Headers", "totalPages");
				responseHeaders.set("Access-Control-Expose-Headers", "totalElements");
				return ResponseEntity.ok().headers(responseHeaders).body(usuarioMapper.toDto(usuariosPage.getContent()));
	}
	
	public void autenticacaoTokenTryCatch(String token) {
		/*if(token.isEmpty()) {
			String msg = "SEM AUTENTICAÇÃO";
			throw new ResponseStatusException(
				           HttpStatus.UNAUTHORIZED, msg, new NotFoundException(msg));
		}	*/
		 try {
	            obterUsuario(token);
	        } catch (Exception e) {
	        	String msg = "Usuario não autorizado";
				throw new ResponseStatusException(
					           HttpStatus.UNAUTHORIZED, msg, new NotFoundException(msg));
	        }
	}
	
	
}
