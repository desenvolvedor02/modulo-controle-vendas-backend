package br.com.telugo.controlevendas.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.telugo.controlevendas.dtos.CriarProdutoDTO;
import br.com.telugo.controlevendas.dtos.ProdutoDTO;
import br.com.telugo.controlevendas.mappers.ProdutoMapper;
import br.com.telugo.controlevendas.models.Produto;
import br.com.telugo.controlevendas.services.ProdutoService;
import br.com.usuario.AutenticadorUsuario;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping(value="/api/produtos")
@Api(value="API REST PARA PRODUTOS")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ProdutoController implements AutenticadorUsuario{
	
	//Dependências
	private ProdutoService produtoService;
	private ProdutoMapper produtoMapper;
	public ProdutoController(ProdutoService produtoService, ProdutoMapper produtoMapper) {
		this.produtoService = produtoService;
		this.produtoMapper = produtoMapper;
	}
	
	/*
	 * ENDPOINT PARA CRIAR PRODUTO
	 * 
	 *@param CriarProdutoDTO
	 *@return ResponseEntity<ProdutoDTO>
	 * */
	@PostMapping("/criar")
    @ApiOperation(value="ENDPOINT PARA CRIAR PRODUTO", response=ProdutoDTO.class)
    public ResponseEntity<ProdutoDTO> criarProduto(
    		@RequestHeader(value = "Authorization") String token,
    		@RequestBody CriarProdutoDTO criarProdutoDTO) {
		this.autenticacaoTokenTryCatch(token);
	   Produto produto = produtoService.criarProduto(criarProdutoDTO); 
	   return ResponseEntity.ok(produtoMapper.toDto(produto));
	}  
	
	/*
	 * ENDPOINT PARA EDITAR PRODUTO
	 * 
	 *@param CriarProdutoDTO
	 *@param long idProduto
	 *@return ResponseEntity<ProdutoDTO>
	 * */
	@PostMapping("/editar/{idProduto}")
    @ApiOperation(value="ENDPOINT PARA EDITAR PRODUTO", response=ProdutoDTO.class)
    public ResponseEntity<ProdutoDTO> editarProduto(
    		@RequestHeader(value = "Authorization") String token, 
    		@RequestBody CriarProdutoDTO criarProdutoDTO, @PathVariable Long idProduto) {
		this.autenticacaoTokenTryCatch(token);
	   Produto produto = produtoService.editarProduto(criarProdutoDTO, idProduto);
	   return ResponseEntity.ok(produtoMapper.toDto(produto));
	}
	
	//Pageable pageable, Long id, String nome, Float valorCompra, Float valorVenda, Integer duracaoEmDias, String fornecedor, Float margemDeGanho
	/*
	 * ENDPOINT PARA LISTAR PRODUTOS DE ACORDO COM FILTROS PASSADOS (ENVIE NULL CASO QUEIRA DESCONSIDERAR)
	 * 
	 *@param CriarProdutoDTO
	 *@param long idProduto
	 *@return ResponseEntity<ProdutoDTO>
	 * */
	@GetMapping("/list")
	@ApiOperation(value="ENDPOINT PARA LISTAR PRODUTOS DE ACORDO COM FILTROS PASSADOS (ENVIE NULL CASO QUEIRA DESCONSIDERAR)",
	response=ProdutoDTO[].class)
	@ApiImplicitParams({
		@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
		@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page.", defaultValue = "5"),
		@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.") 
		})
	public ResponseEntity<List<ProdutoDTO>> listProdutos(
			@ApiIgnore("Ignored because swagger ui shows the wrong params, instead they are explained in the implicit params")
			Pageable pageable,
			@RequestHeader(value = "Authorization") String token, 
			@RequestParam(required=false) Long id,
			@RequestParam(required=false) String nome,
			@RequestParam(required=false) Float valorCompra,
			@RequestParam(required=false) Float valorVenda,
			@RequestParam(required=false) Integer duracaoEmDias,
			@RequestParam(required=false) String fornecedor,
			@RequestParam(required=false) Float margemDeGanho){
		this.autenticacaoTokenTryCatch(token);
		
		Page<Produto> produtosPage = produtoService.buscarProdutos(pageable, id, nome, valorCompra, valorVenda, duracaoEmDias, fornecedor, margemDeGanho);
		//Coloca total de paginas no header
				int totalPages = produtosPage.getTotalPages();		
				long totalElements = produtosPage.getTotalElements();
				HttpHeaders responseHeaders = new HttpHeaders();
				responseHeaders.set("totalPages",
				Integer.toString(totalPages));
				responseHeaders.set("totalElements",
						Long.toString(totalElements));
				responseHeaders.set("Access-Control-Expose-Headers", "totalPages");
				responseHeaders.set("Access-Control-Expose-Headers", "totalElements");
				return ResponseEntity.ok().headers(responseHeaders).body(produtoMapper.toDto(produtosPage.getContent()));
	}
	
	public void autenticacaoTokenTryCatch(String token) {
		/*if(token.isEmpty()) {
			String msg = "SEM AUTENTICAÇÃO";
			throw new ResponseStatusException(
				           HttpStatus.UNAUTHORIZED, msg, new NotFoundException(msg));
		}	*/
		 try {
	            obterUsuario(token);
	        } catch (Exception e) {
	        	String msg = "Usuario não autorizado";
				throw new ResponseStatusException(
					           HttpStatus.UNAUTHORIZED, msg, new NotFoundException(msg));
	        }
	}
	
}
