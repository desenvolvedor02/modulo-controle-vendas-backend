package br.com.telugo.controlevendas.controllers;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.telugo.controlevendas.dtos.CriarLoginDTO;
import br.com.telugo.controlevendas.dtos.CriarProdutoDTO;
import br.com.telugo.controlevendas.dtos.LoginDTO;
import br.com.telugo.controlevendas.dtos.ProdutoDTO;
import br.com.telugo.controlevendas.enums.StatusLogin;
import br.com.telugo.controlevendas.enums.TipoLogin;
import br.com.telugo.controlevendas.mappers.LoginMapper2;
import br.com.telugo.controlevendas.mappers.ProdutoMapper;
import br.com.telugo.controlevendas.models.Login;
import br.com.telugo.controlevendas.models.Produto;
import br.com.telugo.controlevendas.models.Usuario;
import br.com.telugo.controlevendas.services.LoginService;
import br.com.telugo.controlevendas.services.ProdutoService;
import br.com.usuario.AutenticadorUsuario;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping(value="/api/logins")
@Api(value="API REST PARA LOGIN")
@CrossOrigin(origins = "*", maxAge = 3600)
public class LoginController  implements AutenticadorUsuario {
	
	//Dependências
	private LoginService loginService;
	private LoginMapper2 loginMapper;
	public LoginController(LoginService loginService, LoginMapper2 loginMapper) {
		this.loginService = loginService;
		this.loginMapper = loginMapper;
	}
	
	/*
	 * ENDPOINT PARA CRIAR LOGIN
	 * 
	 *@param CriarLoginDTO
	 *@return ResponseEntity<LoginDTO>
	 * */
	@PostMapping("/criar")
    @ApiOperation(value="ENDPOINT PARA CRIAR LOGIN", response=LoginDTO.class)
    public ResponseEntity<LoginDTO> criarLogin(
    		@RequestHeader(value = "Authorization") String token, 
    		@RequestBody CriarLoginDTO criarLoginDTO) {
		this.autenticacaoTokenTryCatch(token);
	   Login login = loginService.criarLogin(criarLoginDTO); 
	   return ResponseEntity.ok(loginMapper.toDto(login));
	}  
	
	/*
	 * ENDPOINT PARA EDITAR LOGIN
	 * 
	 *@param CriarLoginDTO
	 *@param long idLogin
	 *@return ResponseEntity<LoginDTO>
	 * */
	@PostMapping("/editar/{idLogin}")
    @ApiOperation(value="ENDPOINT PARA EDITAR LOGIN", response=LoginDTO.class)
    public ResponseEntity<LoginDTO> editarLogin(
    		@RequestHeader(value = "Authorization") String token,
    		@RequestBody CriarLoginDTO criarLoginDTO, @PathVariable Long idLogin) {
		
		this.autenticacaoTokenTryCatch(token);
		
	   Login login = loginService.editarLogin(criarLoginDTO, idLogin);
	   return ResponseEntity.ok(loginMapper.toDto(login));
	}
	
	/*
	 * ENDPOINT PARA MUDAR STATUS DE LOGIN
	 * 
	 *@param CriarLoginDTO
	 *@param long idLogin
	 *@return ResponseEntity<LoginDTO>
	 * */
	@PostMapping("/editar-status/{idLogin}")
    @ApiOperation(value="ENDPOINT PARA EDITAR STATUS DE LOGIN", response=LoginDTO.class)
    public ResponseEntity<LoginDTO> editarStatusProduto(
    		@RequestHeader(value = "Authorization") String token,
    		@RequestBody StatusLogin status, @PathVariable Long idLogin) {
		
		this.autenticacaoTokenTryCatch(token);
		
	   Login login = loginService.editarStatusLogin(status, idLogin);
	   return ResponseEntity.ok(loginMapper.toDto(login));
	}
	
	//Pageable pageable, Long id, String nome, Float valorCompra, Float valorVenda, Integer duracaoEmDias, String fornecedor, Float margemDeGanho
	/*
	 * ENDPOINT PARA LISTAR LOGIN DE ACORDO COM FILTROS PASSADOS (ENVIE NULL CASO QUEIRA DESCONSIDERAR)
	 * 
	 *
	 *
	 *@return ResponseEntity<ProdutoDTO>
	 * */
	@GetMapping("/list")
	@ApiOperation(value="ENDPOINT PARA LISTAR PRODUTOS DE ACORDO COM FILTROS PASSADOS (ENVIE NULL CASO QUEIRA DESCONSIDERAR)",
	response=LoginDTO[].class)
	@ApiImplicitParams({
		@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
		@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page.", defaultValue = "5"),
		@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.") 
		})
	public ResponseEntity<List<LoginDTO>> listProdutos(
			@ApiIgnore("Ignored because swagger ui shows the wrong params, instead they are explained in the implicit params")
			Pageable pageable,
			@RequestHeader(value = "Authorization") String token,
			@RequestParam(required=false) Long id,
			@RequestParam(required=false) String nome,
			@RequestParam(required=false) Long idUsuario,
			@RequestParam(required=false) Long idProduto,
			@RequestParam(required=false) String nomeUsuario,
			@RequestParam(required=false) TipoLogin tipo,
			@RequestParam(required=false) Date dataCriacaoInicio,
			@RequestParam(required=false) Date dataCriacaoFim, 
			@RequestParam(required=false) Date dataValidadeInicio,
			@RequestParam(required=false) Date dataValidadeFim,
			@RequestParam(required=false) StatusLogin status){
		
		this.autenticacaoTokenTryCatch(token);
		
		Page<Login> produtosPage = loginService.buscarLogins(pageable, id, idUsuario,
				idProduto, nomeUsuario,tipo,
				dataCriacaoInicio, dataCriacaoFim, 
				dataValidadeInicio, dataValidadeFim, status);
		//Coloca total de paginas no header
				int totalPages = produtosPage.getTotalPages();		
				long totalElements = produtosPage.getTotalElements();
				HttpHeaders responseHeaders = new HttpHeaders();
				responseHeaders.set("totalPages",
				Integer.toString(totalPages));
				responseHeaders.set("totalElements",
						Long.toString(totalElements));
				responseHeaders.set("Access-Control-Expose-Headers", "totalPages");
				responseHeaders.set("Access-Control-Expose-Headers", "totalElements");
				return ResponseEntity.ok().headers(responseHeaders).body(loginMapper.toDto(produtosPage.getContent()));
	}
	
	public void autenticacaoTokenTryCatch(String token) {
		/*if(token.isEmpty()) {
			String msg = "SEM AUTENTICAÇÃO";
			throw new ResponseStatusException(
				           HttpStatus.UNAUTHORIZED, msg, new NotFoundException(msg));
		}*/
		 try {
	            obterUsuario(token);
	        } catch (Exception e) {
	        	String msg = "Usuario não autorizado";
				throw new ResponseStatusException(
					           HttpStatus.UNAUTHORIZED, msg, new NotFoundException(msg));
	        }
	}
	
	
}
