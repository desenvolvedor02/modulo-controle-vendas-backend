package br.com.telugo.controlevendas.models;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ValidacaoVenda {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;	
	
	private String nomeBanco;
	private String numeroDocumento;
	private String nomePagador;
	private Float valorPagamento;
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date dataPagamento;
	
}
