package br.com.telugo.controlevendas.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


import br.com.telugo.controlevendas.enums.TipoUsuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Usuario {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;	
	
	private String nome;
	private String cpf;
	private String email;
	private String telefone;
	@Enumerated(EnumType.STRING)
	private TipoUsuario tipo;
	private boolean seParceiroF5;
	private String codigoConsigna;
	private String filial;
	private String cidade;
	private String estado;
}
