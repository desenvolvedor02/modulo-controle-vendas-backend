package br.com.telugo.controlevendas.models;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.telugo.controlevendas.enums.FormaPagamento;
import br.com.telugo.controlevendas.enums.StatusVenda;
import br.com.telugo.controlevendas.enums.TipoLogin;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Venda {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;	
	
	private long idParceiro;
	private String favorecido;
	@Enumerated(EnumType.STRING)
	private FormaPagamento formaPagamento;
	private float valor;
	private String conta;
	private String agencia;
	private String operacao;
	@OneToMany(fetch = FetchType.LAZY)
	private List<Login> logins;
	private String observacao;
	private String comprovantePagamento;
	private StatusVenda status;
	private String numeroDocumento;
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date dataPagamento;
}
