package br.com.telugo.controlevendas.models;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.telugo.controlevendas.enums.StatusLogin;
import br.com.telugo.controlevendas.enums.TipoLogin;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Login {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;	
	
	@ManyToOne
    @JoinColumn(name="usuario_id", nullable=false)
	private Usuario usuario;
	
	@ManyToOne
    @JoinColumn(name="produto_id", nullable=false)
	private Produto produto;
	private String nomeUsuario;
	@Enumerated(EnumType.STRING)
	private TipoLogin tipo;
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date dataCriacao;
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date dataValidade;
	@Enumerated(EnumType.STRING)
	private StatusLogin status;
}
