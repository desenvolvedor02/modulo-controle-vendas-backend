package br.com.telugo.controlevendas.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Produto {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;	
	private String nome;
	private float valorCompra;
	private float valorVenda;
	private int duracaoEmDias;
	private String fornecedor;
	
	public float getMargemDeGanho() {
		return this.valorVenda - this.valorCompra;
	}
}
